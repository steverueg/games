#!C:/Users/steve/anaconda3/python.exe
#  - for py cgi: add 2 lines to end of xampp/apache/conf/httd.conf
#      AddHandler cgi-script .py
#      ScriptInterpreterSource Registry-Strict

# test with: 
#  python api/cgi args:  https://stackoverflow.com/questions/25336688/run-python-script-from-ajax-or-jquery
#  (datascience) C:\Users\steve\src\games\tic-tac-toe>python.exe t3aas.py b=101......

import os, sys, random
import cgi, cgitb

import t3aas_lib as lib

verbose = True



# ----------------------------------------------
# get current game initilization parameters
# ----------------------------------------------
data = cgi.FieldStorage()
boardStr = data.getvalue('b', '')
h = data.getvalue('h','1')

# setup API
if (h != '0'):
    print("Content-Type: text/html")
    print()


# valid board string
if len(boardStr) != 9: 
    print("-1")
    sys.exit()


# ----------------------------------------------
# board setup
# ----------------------------------------------
board = lib.ajax2board(boardStr)
if verbose: lib.printBoard(board)

# ----------------------------------------------
# playSpace btree setup
# ----------------------------------------------
# get the game from this point down 
playSpace = lib.nextMove(board)
playSpaceHash = lib.space2Hash(playSpace)
if verbose: print("length of playSpace = "+str(len(playSpace)))

# ----------------------------------------------
# what are my next move options - children nodes of top nodeid 
# ----------------------------------------------
myNextMoves = playSpaceHash[1][12]
if verbose: print("my next moves are: "+str(myNextMoves))

# ----------------------------------------------
# get a score metric - for each of my next moves
# ----------------------------------------------
nodeScores = {}
myWinningMoves = []     # list of nodes where I've won (next move about to take)
thisMoveLooses = {}     # a hash of the myMoves nodes that cause an immediate loss (once opp pounces on me)

for n in myNextMoves:
    b = playSpaceHash[n]       # child board - what board looks like with move option n

    # ----------------------------------
    #  special -- immediate -- must do next moves
    # ----------------------------------
    #  a. can I win now?
    if b[13] is not None and b[13] > 0: myWinningMoves.append(n)

    #  b. can I loose now? i.e. can the opponent win next move? 
    #     loop over the opponents next moves after my move n
    thisMoveLooses[n] = lib.thisMoveLooses(n, playSpaceHash)

    # ----------------------------------
    # regular scoring
    # ----------------------------------
    # 1. for a given node (next move), get a flat list all its potential nodes from tree
    plays = lib.childNodeSource(n, playSpaceHash)

    # 2. for all those nodes,  get the board structures
    boards = lib.node2board(plays, playSpaceHash)

    # 3. for all those board structures, map into a simple list of raw scores about that board
    rawScores = [ lib.boardMapper(b) for b in boards ] 

    # 4. now reduce that list of raw scores (and levels) into a single metric score
    #    so we can make a decision 
    nodeScores[n] = lib.scoreNodeReducer(rawScores)


# ----------------------------------------------
# process those metrics
# ----------------------------------------------

# sort generic node ids (keys) by descending scores 
nodess = sorted(nodeScores, key=lambda i: nodeScores[i], reverse=True)


# process nodes for blocks needed
#  - any of these a 1?  If so, then any None's?
blockNeeded = sum(thisMoveLooses.values()) > 0    # boolean
blockMoves = []                                   # any moves that have 0 losses?
if blockNeeded:
    # current moves with 0 n+1 losses is a 'block'
    blockMoves = list(filter(lambda k: thisMoveLooses[k]==0, thisMoveLooses.keys() ))
blockMoves.sort(key=lambda i: nodeScores[i], reverse=True)    # in case more than 1


if verbose:
    print("Metrics:")
    print(" winning moves: "+str(myWinningMoves) )
    print(" loosing moves list (oppWin n+1): "+str(thisMoveLooses))
    print("    Is there a block move I must take now (or loose)? "+str(blockNeeded))
    print("    Does any moves block a loss so that there is 0 oppWins? "+str(blockMoves))
    if (blockNeeded and blockMoves==[]):
        print("    Dude, you can't stop opponent.  They win.  You lose.")
    print(" All nodes general score:")
    print("\n".join( [f"   node #{k} -> score={v:0.01}" for k,v in nodeScores.items()] ))
    print("   ergo - sorted nodes recommendation: "+str(nodess))

recommendedNode = None

if len(myWinningMoves) > 0:
    if verbose: print("* for the win")
    recommendedNode = myWinningMoves[0] if len(myWinningMoves) == 1 else random.choice(myWinningMoves)

elif len(blockMoves) > 0:
    if verbose: print("* for the best block of oppWin")
    recommendedNode = blockMoves[0] if len(blockMoves) == 1 else random.choice(blockMoves)

elif (blockNeeded and blockMoves==[]):
    if verbose: print("* we are going to loose... but let's block something!")
    blockingNodes = list(filter(lambda x: thisMoveLooses[x]==1, thisMoveLooses.keys()))
    recommendedNode = random.choice(blockingNodes)

else:
    if verbose: print("* the best score examining all possible moves in play space")
    max = nodeScores[nodess[0]]
    choiceList = nodess
    if max != 0.0:
        delta = max * 0.10
        choiceList = list(filter(lambda x: (max - nodeScores[x]) < delta, nodess))
        if verbose: print(f"   max={max}, delta={delta:0.01}, choiceList={choiceList}")
    recommendedNode = random.choice(choiceList)


if verbose: print("the recommended node is #"+str(recommendedNode)+" which is cell...")
print(playSpaceHash[recommendedNode][14])
sys.exit()
