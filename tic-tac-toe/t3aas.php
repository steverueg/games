<?php 
/* python wrapper
    - bitnami lamp does not include apache mods for python
    - that's fine... its actually a security
    - so, we'll use php to wrap the python t3aas

# test with: 
#  (datascience) C:\Users\steve\src\games\tic-tac-toe>python.exe t3aas.py b=101......

*/

# project root
$project = "/opt/projects/games/tic-tac-toe";

# get board string
$b = $_GET['b'] ?? null;

system("/usr/bin/python3 $project/t3aas.py 'h=0&b=$b'");

?>