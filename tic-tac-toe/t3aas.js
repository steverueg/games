/* javascript for ajax capabilities*/


player = "X";      // current player, js global, user starts as X
gameOver = false;
level = 0;

/* --------------------------------------------------- 
  the user selected a move
   --------------------------------------------------- */
function enterTurn(id) {
    // accept players turn, update player char, call ajax
    if (gameOver) { return; }    // just ignore clicks if game over

    // check what was clicked on
    var clickedCell = document.getElementById(id);

    // is it empty?  if empty then change else ignore
    //   Note: we do not change players when using t3aas
    //         the user is always 'X' and the computer (api) is always 'O'
    if (clickedCell.innerHTML !== " ") { return; }

    bgReset();
    clickedCell.innerHTML = player;       // change the board
    level = level + 1;
    console.log("level "+level+": enterTurn("+id+") for X");

    log = document.getElementById('log');
    log.innerHTML = "Your move to square "+id+"<br>";

    // did that click win the game?
    iwon = didPlayerWin(1);       // X = 1
    if (iwon) { 
        gameOverMsg("You Won!");
        return; 
    }

    // was that the last move?
    if (level == 9) {
        gameOverMsg("Tie.  Cat's game.");
        return; 
    }

    // ----------------------------------------------- 
    // computer response to that play 
    // ----------------------------------------------- 

    // get status of board
    var boardStr = getBoard();
    var URL = "t3aas.php?b="+boardStr;
    // console.log("URL="+URL);

    // make the API call to T3AAS
    //  -- wait for recommendation to be returned 
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // send the text to the log <span>
            log.innerHTML = xhr.response;
            // get the actual recommended apiCell # -- the last line
            var loglines = xhr.response.split("\n");
            var apiCell = loglines[loglines.length - 2].trim();
            if (apiCell == '-1') {
                console.log("recommended API cell is null");
                return;
            }
            
            // --------------------------------
            //  make the move for the computer
            // --------------------------------
            //   Note: we do not change players when using t3aas
            //         the user is always 'X' and the computer (api) is always 'O'
            level = level + 1;
            console.log("level "+level+":  t3aas move is cell #"+apiCell);
            document.getElementById(apiCell).innerHTML = 'O';
            document.getElementById(apiCell).style.backgroundColor = "#DDD";

            // did that click win the game?
            ilost = didPlayerWin(0);       // O = 0
            if (ilost){
                gameOverMsg("You lost.");
                return; 
            }

        }
    };

    xhr.open("GET", URL, true);
    xhr.send();

}


function bgReset() {
    // remove last bgcolor, by resetting all 9
    for (let i = 0; i < 9; i++) {
        document.getElementById(i).style.backgroundColor="#FFF";
        } 
}

function getBoard() {
    // convert the HTML board to a string of .01
    var boardStr = '';
    for (let i = 0; i < 9; i++) {
        var c = document.getElementById(i).innerHTML;
        if (c==" "){ boardStr += ".";}
        if (c=="X"){ boardStr += "1";}
        if (c=="O"){ boardStr += "0";}
      } 
    console.log("boardStr="+boardStr);
    return boardStr;
}

function getBoardArray() {
    // convert the HTML board to a string of .01
    var board = [];
    for (let i = 0; i < 9; i++) {
        var c = document.getElementById(i).innerHTML;
        if (c==" "){ board.push(null);}
        if (c=="X"){ board.push(1);}
        if (c=="O"){ board.push(0);}
      } 
    console.log("boardArray="+board);
    return board;
}


function didPlayerWin(p) {
    // check the board to see if there is a winnder
    b = getBoardArray();  // the 01. string
    win = false;
    if (b[0]==p && b[3]==p && b[6]==p){ win=true; winRow(0,3,6);}
    if (b[1]==p && b[4]==p && b[7]==p){ win=true; winRow(1,4,7);}
    if (b[2]==p && b[5]==p && b[8]==p){ win=true; winRow(2,5,8);}
        
    if (b[0]==p && b[1]==p && b[2]==p){ win=true; winRow(0,1,2);}
    if (b[3]==p && b[4]==p && b[5]==p){ win=true; winRow(3,4,5);}
    if (b[6]==p && b[7]==p && b[8]==p){ win=true; winRow(6,7,8);}
        
    if (b[0]==p && b[4]==p && b[8]==p){ win=true; winRow(0,4,8);}
    if (b[2]==p && b[4]==p && b[6]==p){ win=true; winRow(2,4,6);}

    console.log("didPlayWin p="+p+" and w="+win);
    if (win) { 
        gameOver = true;
        console.log("The game is over.");
    }
    return win;
}


function winRow(a,b,c) {
    // highlight the cells of the winning row
    document.getElementById(a).style.backgroundColor = "#DED";
    document.getElementById(b).style.backgroundColor = "#DED";
    document.getElementById(c).style.backgroundColor = "#DED";
}


function changePlayer() {
    // switch the js global var to the other char
    if (player == "X"){ player = "O";}
    else { player = "X";}
    console.log(" new player = "+player);
}


function gameOverMsg(msg){
    // when the game is over, show a message and a button
    gameOver = true;
    document.getElementById('msg').innerHTML = msg;
    document.getElementById('pa').innerHTML = "<form><button class='btn btn-success'>Replay</button></form>";
}

