#  library for t3aas

verbose = True

# -------------------------------------- 
#  init
# -------------------------------------- 
# a board: 3x3 cells +  level  +  node id +  parent node  + children + #wins + cell = list of size 15
#            0-8          9         10            11          12         13     14

def newEmptyBoard():
    # new board structure - a list of 14 elements
    emptyBoard = [ None ] * 15   # board structure 
    emptyBoard[9] = 0            # the only level=0 board 
    emptyBoard[10] = 0           # root node (no parent)
    emptyBoard[12] = []          # list of children not defined yet
    return emptyBoard



# -------------------------------------- 
#  B-tree playSpace generation
# -------------------------------------- 

def nextMove(board):
    # given a single board, make the next 9 moves = 9 more boards, append #Wins at end
    #  - (vaild) skipping if box/cell location already played
    #  - (valid) mark if gameOver for next level (truncate branch)
    #  - https://www.codecademy.com/learn/learn-recursion-python/modules/recursion-python/cheatsheet
    global currNodeId, verbose 
    
    playSpace = [ board ]                  # this is where all the final results will go
    
    # a board: 3x3 cells +  level  +  node id +  parent node  + children + #wins + cell = list of size 15
    #            0-8          9         10            11          12         13     14

    # inheritance - things all child nodes (next moves) have
    #   - conversion of this level to next level
    parentNodeId = board[10]
    level = board[9] + 1            # we increment the level here from parent to child level
    player = level % 2              # player 1 goes first, on odd levels (level = # moves)

    # if parentNodeId % 100 == 0 : print(f"new level={level},parent={parentNodeId},b={board}") # debug

    # loop over all cells on the board: 3x3=9
    #   - if level <= 9, there is at least 1 play left 
    for i in range(0,9):
        if board[i] != None: continue       # skip if player already moved there
      
        # create new node
        node = newEmptyBoard()               # copy parent node as starting point (deep copy = list of lists)
        node[0:9] = board[0:9]               # I think a slice is a value copy
        node[14] = i                         # which cell was just changed?
        node[i] = player                     # 1 new move from player in function args
        node[9] = level                      # next level after parent level
        node[11] = parentNodeId              # inherit
        currNodeId += 1                      # global counter pre-increment
        node[10] = currNodeId                # new (child) node id in this node
        board[12].append(currNodeId)         # add child node id to the parent board
        if level >= 5:
            node[13] = didPlayerWin(node)  # can current (child) move win?
            
        if verbose and level == 1: 
            print(f"\n\n* back to level={level}, child={i},currNodeId={currNodeId}, node={node}") # debug
            print(f"   - and parent board={board}")

        # --------------------------------- 
        # recursion! get and append the children of the next level from this node
        # --------------------------------- 
        #  - how does a game end???? (no next level)
        #     1) cat's game (all 9 moves done)
        #     2) player won - no need to continue
        if level <= 9 and (node[13] in (None, 0)):
            playSpace.extend( nextMove(node) )
        else:
            playSpace.append( node )                # no more moves, game over, end this branch

    return playSpace


# turn the branch list-of-list play space into a hash for lookup
# - the hash is a fast b-tree lookup
def space2Hash(playSpace):
    # return a node based hash to use a b-tree quick search of playspace
    playSpaceHash = {}
    for b in playSpace:
        playSpaceHash[b[10]] = b     # key = nodeid, value = board list
    return playSpaceHash


def didPlayerWin(b):
    # Did player p win on this current board b
    # p assumed to have last move, which altered the board
    p = b[9]%2  # player is mod2 the level (1 goes first on level 1)
    win=0
    if (b[0]==p and b[3]==p and b[6]==p): win+=1  # V
    if (b[1]==p and b[4]==p and b[7]==p): win+=1
    if (b[2]==p and b[5]==p and b[8]==p): win+=1
        
    if (b[0]==p and b[1]==p and b[2]==p): win+=1  # H
    if (b[3]==p and b[4]==p and b[5]==p): win+=1
    if (b[6]==p and b[7]==p and b[8]==p): win+=1
        
    if (b[0]==p and b[4]==p and b[8]==p): win+=1  # D
    if (b[2]==p and b[4]==p and b[6]==p): win+=1
    return win



# -------------------------------------- 
#  helper functions
# -------------------------------------- 
def printBoard(b):
    # given a board, print a little ascii version
    print(" %4s %4s %4s"%(b[0],b[1],b[2]))
    print(" %4s %4s %4s"%(b[3],b[4],b[5]))
    print(" %4s %4s %4s"%(b[6],b[7],b[8]))
    print(f"node={b[10]}, parent={b[11]}, children={b[12]}, # Wins={b[13]}")


# searches an entire playSpace for 1 node -- Yuck 
def getBoard(nodeId ,playSpace):
    # given a playSpace and a nodeId, return the board list
    for b in playSpace:
        if b[10]==nodeId: return b
    return []


# -------------------------------------- 
#  getting scores
# -------------------------------------- 
# MR programming functionality
#   Map/Reduce
#    mapper - process the raw data to the key and value arguments
#    reducer - aggregate values by key 

def node2board(nodeList, playSpaceHash):
    # convert a list of NodeIds to a list of Board structures
    boardList = [ playSpaceHash[n] for n in nodeList ]
    return boardList

def boardMapper(b):
    # given a board list, return key=level & value=[win1, win2]
    # [ int, bool, bool ]
    r = [ b[9], b[13]==1, b[13]==2 ]     
    return r

# this is a SOURCE to scoring
#   source -> map -> reduce 

def childNodeSource(startNode, playSpaceHash):
    # for a node during any game,
    # recursively call all nodes in btree hash and return a flat list with level=key as first element
    nodeList = [ startNode ]    # the final flat-list result
    
    # loop over children (maybe none none)
    for childNode in playSpaceHash[startNode][12]:
        nodeList.extend( childNodeSource(childNode, playSpaceHash) )

    return nodeList


def scoreNodeReducer(rawScoreList):
    # given a list of raw scores for a node (and all children) aggregate (reduce) to a single metric
    #   raw score = [ level, win1, win2 ]  where winx is boolean

    # I win 1, I win 2, I loose 1, I loose 2
    w = [ 1,100,1,200 ] 
    
    # assume level of first element is my next move
    level = rawScoreList[0][0]
    player = level % 2                # player = 1/0
    
    metricWin = 0
    metricLoose = 0
    cat = 0
    for s in rawScoreList:
        [lvl, w1, w2] = s
        p = lvl%2
        if p == player:
            if w1: metricWin += 1*w[0]
            if w2: metricWin += 2*w[1]
        else:
            if w1: metricLoose += 1*w[2]
            if w2: metricLoose += 2*w[3]
        if lvl == 9 and not w1 and not w2: cat += 1
            
    # win to loss ratio as a final metric
    if metricLoose == 0: ratio = metricWin
    else: ratio = metricWin / metricLoose
    
    percent = metricWin / (metricWin + metricLoose + cat)
    #print(f"curent level = {level}, player={player}, w={metricWin}, l={metricLoose}, cat={cat}, r={ratio}, p={percent}")
    
    return percent


def thisMoveLooses(n, playSpaceHash):
    "given a node and a playSpace, does the opponent win in any move after -- do I need to block now"
    b = playSpaceHash[n]
    oppWins = 0
    # loop over the opponent moves
    for oppMove in b[12]:
        b2 = playSpaceHash[oppMove]
        if b2[13] not in (None, 0): 
            # whoa, the opponent (2 plays from now) can win! 
            oppWins += 1     # this parent did NOT block this oppWin! 
    return oppWins



# -------------------------------------- 
#  ajax cgi functions
# -------------------------------------- 
currNodeId = 1000

def ajax2board(boardStr):
    "given the boardStr from ajax call, return the standard board structure"
    global currNodeId, verbose

    # here is what a board (structure) looks like
    # a board: 1x9 cells +  level  +  node id +  parent node  + children + #wins = list of size 14
    #            0-8          9         10            11          12         13
    board = newEmptyBoard()
    
    level = 0
    # TTT board: 0..8
    for i in range(0,len(boardStr)):
        c = boardStr[i]
        if c == '.': 
            board[i] = None
        else:
            board[i] = int(c)
            level += 1

    board[9] = level
    board[10] = 1    # I just want empty board to be nodeid=0, else nodeid=1

    if verbose: print('level='+str(level)+' board='+str(board))

    return board
