#!C:/Users/steve/anaconda3/python.exe
#!/usr/bin/python3 
#  library for wordly
import random

# -------------------------------------- 
#  init
# -------------------------------------- 

verbose = False
filename = "dictionary.txt"
alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def loadDictionary():
    """given a filename, load 5-letter words into a list"""
    global filename
    words = []      # a list 
    fp = open(filename)
    for w in fp.read().split('\n'):
        if len(w) == 5: words.append(w)    # keep 5-letter words
    fp.close()
    return words


# -----------------------------------------------
# metric score: char-position probabilities
# -----------------------------------------------
# a dictionary where key is alphabet and value is empty list of 5 zeros
def charPositionProb(solutionSet):
    "given a solution set of 5-char words, count # each char at each position"
    global alphabet
    charProb = {}                  # a hash of lists
    for key in alphabet:
        charProb[key] = [0]*5      # each char key's list init to all 0's

    # loop over all words
    # - count the number of chars in each of the 5 positions
    for word in solutionSet:
        for position in range(0,5):
            char = word[position]
            charProb[char][position] += 1
    
    return charProb


def scoreCharProb(solutionSet, charProb):
    "give a score to a solution-set based on char-position-probabilty of another solution subset "

    score = {}   # dict of scores, key=word, value=score
    for ans in solutionSet:
        score[ans] = 0
        for i in range(0,5):
            c = ans[i]    # char in word 
            score[ans] += charProb[c][i]

    # create a sorted list from the dict
    #  - result = sorted list of tuples
    scoredSolutions = sorted(score.items(), reverse=True, key=lambda x: x[1])
    return scoredSolutions


# -----------------------------------------------
# metric score: optimization MAX of MINs
# -----------------------------------------------
def scoreMiniMax(gameState, answerSet, p=-1, t=None, m=None):
    """loop over guessSet as g
         loop over answerSet as a
            evalute result of g,a -- pretend g is the guess and a is the answer
            then calcuate the new solutionSubset from that pretend guess,ans
            score is the # answers REMOVED
         get min of all simulations for that G
       final recommendation is the max of all the guesses 
    """
    numAns = len(answerSet)
    minScore = {}                         # dict: key=a guess, value=its score
    i = 0
    
    if verbose:
        print("There are "+str(numAns)+" words in the answerSet")
        if (p > 1): print(f" special case: m={m}, p={p}, t={t}")
    
    # loop over the set of all guesses
    #  - could be all 5-letter words in Dict
    #  - but that's a big problem space
    #  - so, lets only use possible answers
    for g in answerSet:
        i += 1
        if (i % 50 == 0): print("guess #"+str(i))

        # special case: parallel threads for first-guess scenario
        if (p > 1 and m == '4') and ((i % p) != t): continue

        guessMinScore = {}                         # hash of scores

        for a in answerSet:
            if a == g: continue
            # simulate this pretend game with guess g and answer a
            simResult = evaluate_guess(g, a)       # evaluate this guess IF the answer was a
            simState = aggResultSet([gameState, simResult])     # what is the new (simulalted) State
            simAnswers = getSolutionSubset(simState, answerSet) # what would the next possible answers be?
            score = numAns - len(simAnswers)       # how many current poss ans removed?
            guessMinScore[a] = score

        # for all the avail answers get a list by score
        guessScoreList = sorted(guessMinScore.items(), key=lambda x: x[1])
        minScore[g] = guessScoreList[0][1]
        #print("guess="+g+" has min score="+str(minScore[g]))

    # which guess is best?
    #  get Max of those min scores -> revserse sort
    scoreList = sorted(minScore.items(), reverse=True, key=lambda x: x[1])
    #print("scoreList="+str(scoreList))

    return scoreList
    



# -------------------------------------- 
#  evaluate_guess
# -------------------------------------- 
def evaluate_guess(guess, solutionStr):
    "guess is a 1 string current guess, solutionStr is The Game Answer"

    # copy the solution to an array, so we can REMOVE char elements for counting
    answer = list(solutionStr)         # string to array 
    
    # 1. exact match w/ correct position 
    match = [None]*5  
    for i in range(0,5):
        #  1:1, n:n, position-to-position matching
        match[i] = (guess[i] == solutionStr[i])      # list of Boolean flags
        if match[i]: answer[i] = None                # remove char from answer - it's been matched

    # 2. is a guessed char somewhere in the answer (included, but not matched -- or not exist)
    included = [None]*5                # char is in the answer, but not in correct position
    # loop over 5 chars of THIS GUESS
    for i in range(0,5):
        if match[i]: continue          # this position already matches

        # included?
        for j in range(0,5):
            if guess[i] == answer[j]:
                included[i] = True
                answer[j] = None       # remove that match from the answer ARRAY for next round

    # 3. char not included in answer
    naughty = []
    for g in guess:
        if (g not in solutionStr and g not in naughty): naughty.append(g)
    
    # result short hand
    #  - format: 5-char regex | nice | naughty
    resultStr = "".join( [ guess[i] if (match[i]) else '.' for i in range(0,5) ] )
    resultStr += "|"
    resultStr += "".join( [ guess[i] if (included[i]) else '' for i in range(0,5) ] )
    resultStr += "|"
    resultStr += "".join( naughty )

    if False:
        print("solution="+solutionStr)
        print("guess ="+guess)
        print("match ="+",".join([ str(x) for x in match ]))
        print("included="+",".join([ str(x) for x in included ]))
        print("naughty = "+str(naughty))
        print("resultStr = '"+resultStr+"'")

    return resultStr


# -------------------------------------- 
#  Aggregate N result sets into 1 Game State
# -------------------------------------- 
# - duplicates are TOUGH --> There is a priority for each char
# - Priority: 1) match, 2) nice (OOP - out of position), 3) then we say naughty
# - state=.....|AS|NAL ;  the 1st A is nice, there is a 2nd A not there (which is good info!)
# -       .....|EED|NY
def aggResultSet(resultSet):
    "return single resultState from the N plays"
    match = [ '.' ]*5     # 5-element regex: char or .
    nice = {}             # hash of chars in answer but out-of-position, dups allowed (non-unique)
                          #  e.g. key=E, value=# counts (int)
    naughty = []          # list of unique chars, where this Nth+ char (not in match or nice) is not allowed


    # loop over the plays, results, so far in the game
    for r in resultSet:
        # result format is 5-char matched string | chars contain in incorrect position
        (matchIN,niceIN,naughtyIN) = r.split('|')        # INCOMING e.g. A....|E|RIS

        # 1. aggregate match, position-by-position
        for i in range(0,5):
            c = matchIN[i]
            if c != "." and match[i] != "." and match[i] != c:
                # different chars said to match same position
                raise StateAggError("There are conflict in char Matches")
            if c != ".":
                match[i] = c
                # the out-of-position char (nice) from a prev result is now known to be in position (a match)
                if c in nice and nice[c] > 0: nice[c] -= 1  # oh, this char is matched
        #print("match="+str(match))


        # 2. build list of nice chars (not unique, dups available)
        #  - since dups are allowed, we need to count the occurances in dict
        #  - in aggregation, we need two hashes, one global and one local
        #  - these both will be very small
        niceLocal = {}             # from IN only; key=char, value=integer count
        for c in niceIN:
            if c not in niceLocal.keys(): niceLocal[c] = 1
            else: niceLocal[c] += 1
        # now, the aggregation
        for c in niceLocal:
            if c not in nice: nice[c] = niceLocal[c]
            else: nice[c] = max(nice[c], niceLocal[c])
        #print("nice="+str(nice))


        # 3. build hash of naughty chars (c is unique)
        #    - if a user told us a char was not in -- then it must remain that way
        #    - but a this could be a 2nd not in char, so we don't need to test match or nice
        #    - no dups, once a naughty, past match and nice, then all N+ are still naughty
        for c in naughtyIN:
            if c not in naughty: naughty.append(c)
        #print("naughty="+str(naughty))

    # make the state into a String
    #  - same format as individual resultSet
    resultState = "".join( match )
    resultState += "|"
    resultState += "".join( [ str(c)*nice[c] for c in nice ] )  # key='E' & value=2 => EE
    resultState += "|"
    resultState += "".join( naughty )

    return resultState


class StateAggError(Exception):
    "there is a conflict aggregating all responses into 1 state"
    pass


# -------------------------------------- 
#  Find all possible solutions (subset) given a game State
# -------------------------------------- 

def getSolutionSubset(resultState, solutionSet):
    "subset of viable, possible answers, given the game state - aggregation of all N plays results"
    solutionSubset = []          # list of possible game solutions (answer)

    # setup data structures for game state
    (matchStr,niceStr,naughtyStr) = resultState.split('|')        # e.g. A..TE|B|RISX
    
    # -------------------------------------------
    # loop over all 5-char words from dictionary
    # -------------------------------------------  
    for w5 in solutionSet:
        # we check words in priority-clues:
        #   1. match chars
        #   2. nice chars
        #   3. naughty chars (after match or nice)

        # make a template - remove match and nice chars as they come up
        template = list(w5)      # mutable list to 'deal' with dups

        # 1. check for match pattern 
        boolMatch = True
        if matchStr != '.....':
            for i in range(0,5):
                if matchStr[i] != '.':
                    if template[i] == matchStr[i]: template[i] = None
                    else: boolMatch = False

        if not boolMatch: continue


        # 2. check for contain but out-of-position (dups allowed) nice
        #    - the possible answer must contain ALL nice chars
        boolContain = True       # are all chars contained?  1 negative trips bool flag negative
        for c in niceStr:
            if c not in template: 
                boolContain = False          # flip flag to false
            else: 
                pos = template.index(c)      # position of first char
                template[pos] = None
        
        if not boolContain: continue          # if false, do not append
            
        # 3. naughty chars cannot exist in the solution --> after match and nice
        #     - match and nice chars are removed -- the remaining chars in template cant be naughty
        boolNaught = False
        for c in naughtyStr:
            if c in template: boolNaught = True
        
        if boolNaught: continue
        
        
        # if we get here - this is a valid possible solution
        solutionSubset.append(w5)
        
    return solutionSubset



# -------------------------------------- 
#  First guess
# -------------------------------------- 
def firstGuess(possibleAnswers):
    "need different algo for first guess:  not 4000*4000 search"
    return random.choice(possibleAnswers)


# -------------------------------------- 
# from all solution, get subset of possible recommendations
# -------------------------------------- 
#  - return a list of the top guesses: from which to select 1 recommendation
def topGuesses(scoredSolutions):
    "answers are tuples of (word, score). Return the sub-list of top guesses worthy of recommendation"

    topScore = scoredSolutions[0][1]
    minScore = int(topScore * 0.90)  # int() is a ceiling() always ROUNDS UP to the next highest int with any remainder
    scoredRecommendations = [ s for s in scoredSolutions if (s[1] >= minScore) ]
    return scoredRecommendations

