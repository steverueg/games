#!C:/Users/steve/anaconda3/python.exe
#!/usr/bin/python3
# ---------------------------------------
# this gives a recommendation for the next WORDLE play
# ---------------------------------------
# inputs:
#  - m = method: 1=char-position probability; 2=minimax
#  - n = round #
#  - state = 3-part string of the current game state from the aggregation of all previous plays
#  - result = the 3-part string of the last N-1 play
#
# output: the recommended next guess
#
#  for py cgi: add 2 lines to end of xampp/apache/conf/httd.conf
#      AddHandler cgi-script .py
#      ScriptInterpreterSource Registry-Strict
#
# test with: 
#  python api/cgi args:  https://stackoverflow.com/questions/25336688/run-python-script-from-ajax-or-jquery
#  (datascience) C:\Users\steve\src\games\wordly>python wordly.py "state=..A..||PLY"

import cgi, json, random
import wordly_lib as lib
from wordly_lib import StateAggError

verbose = True

# ----------------------------------------------
# init 
# ----------------------------------------------
apiSend = {}      #  What we send to HTML/JS in json format
apiSend['Error'] = False


# 1. get current game initilization parameters
data = cgi.FieldStorage()

# 1a. setup API 
h = data.getvalue('h', '1')
if (h != '0'):
    print("Content-Type: text/html")
    print()

# 1b. round
#   - start of game is round #1
n = int(data.getvalue('n', '1'))
apiSend['round'] = n
if verbose: print("round="+str(n))


# 1c. what metric to use for getting the next guess?
#  1 = char-position probability occurance
#  2 = optimizaiton minimax algo 
#  3 = first round
m = data.getvalue('m', '2')
if verbose: print("method="+str(m))
if m not in ['1','2','3']:
    print("Error, method is not valid")
    exit(2)
    

# 1d. get previous game state
#  format: 5-char regex | included-out-of-position | excluded
state = data.getvalue('state', '').upper()
if state == '': state= '.....||'            # init new game
if verbose: print("state="+str(state))


# 1e. get previous RESPONSE to the guess cmp to the (unknown) answer
response = data.getvalue('response', '').upper()
if response != '':
    try:
        # update the aggregated current game state for this round
        state = lib.aggResultSet([state, response])
        if verbose: print("response="+response+"; new state="+state)
    except StateAggError as e:
        print("There was conflict in game responses: "+e.args[0])
        apiSend['ErrorType'] = "game State conflict"
        apiSend['Error'] = True
        apiSend['ErrMsg'] = e.args[0]
        print(json.dumps(apiSend))
        exit(5)


# --------------------------------
# special case for game start
# --------------------------------
# - special text file created ONCE from this script
# - sorted by best guesses on top
# - only read in the top N-% age
if n == 1 or state == '.....||':
    m = '3'
    fp = open("firstguess.all.txt")
    line1 =  fp.readline()
    w1, maxScore = line1.split(',')
    firstGuess = [w1]                       # a list of 1st guesses
    for line in fp.read().split('\n'):
        w, s = line.split(',')
        if (float(s) > float(maxScore) * 0.98): firstGuess.append(w)
        else: break
    fp.close()
    if verbose: print("1st-guesses = "+str(firstGuess))
    apiSend['numRecommendations'] = len(firstGuess)
    apiSend['algo'] = '1st-Guess'
    apiSend['recommendation'] = random.choice(firstGuess)
    apiSend['state'] = state
    apiSend['m'] = m
    print(json.dumps(apiSend))
    exit(0)


# 1f. special case: are we doing parallel threads for first-guess scenario?
#   p = # partitions total
#   t = this particular thread #
p = int(data.getvalue('p', -1))
t = int(data.getvalue('t', -1))
if (p > 1 and n == 1): 
    m = '4'    # (str of an int) special parallel-thread first-guess
    print(f"special first-guess parallel-thread case: p={p}, t={t}, m={m}")



# 1g. what api responds back to HTML in user's game
apiSend['state'] = state
apiSend['m'] = m
    
# 1h. very unique edge case
#     python wordly.py "n=2&state=A....|IRES|&response=ARI..|ES|"
possArrow = int(data.getvalue('possArrow', '0'))




# 2. read in 5-letter words from dictionary
#    - a list
solutionSet = lib.loadDictionary()
if verbose: 
    print("loaded "+str(len(solutionSet))+" 5-letter words (upcase) into a dict")


# ----------------------------------------------
# find solution set of possible secret words
# ----------------------------------------------
#  - based on this new state

# 3. get the list of viable, possible solutions
possibleAnswers = lib.getSolutionSubset(state, solutionSet)
npa = len(possibleAnswers)
if verbose:
    print("There are "+str(npa)+" possible answers")
    print("Unscored solution subset (first 10) = "+str(possibleAnswers[0:min(10,npa)]))
apiSend['numAns'] = npa


# special situations based on possible solution for this new state
if npa == 0:
    print("Error: I have no words -- no possible answers for these responses.")
    apiSend['ErrMsg']= "Error: I have no words -- no possible answers for these responses."
    apiSend['ErrorType'] = "no possible Answers"
    apiSend['Error'] = True
    print(json.dumps(apiSend))
    exit(6)

if npa == 1:
    print("I have only 1 solution for you.  I hope this is correct.")
    apiSend['numRecommendations'] = npa
    apiSend['recommendation'] = possibleAnswers[0]
    print(json.dumps(apiSend))
    exit(7)




# ----------------------------------------------
# from that game state & solution set, get a next word recommendation
# ----------------------------------------------
if m in ['1']:
    apiSend['algo'] = 'charPosProb'
    # 5. get char-position-probabilities, BASED ON the solution Subset
    # 6. score all words in dict based on words reveal the most char-pos info
    #    - solution set is all dict
    #    - char-pos-prob is from current State solution subset
    charPosProbHash = lib.charPositionProb(possibleAnswers)
    scoredSolutions = lib.scoreCharProb(possibleAnswers, charPosProbHash)
    if verbose:
        print("charPosProbHash['A']="+str(charPosProbHash['A']))

elif m in ['2','4']:
    # '2' is regular, '4' is parallel-thread 
    apiSend['algo'] = 'minimax'
    # optimization minimax
    scoredSolutions = lib.scoreMiniMax(state, possibleAnswers, p=p, t=t, m=m)
    if (p > 1 and n == 1): 
        # if special first-guess parallel-thread case, save all output to a thread-file
        with open('firstguess.'+str(t)+".txt", 'w') as f:
            f.writelines( [ f"{t[0]},{t[1]}\n" for t in scoredSolutions ] )  # tuples

else:
    print("improper method m="+str(m))
    exit(3)

if verbose:
    print("Scored Solutions (first 10) = "+str(scoredSolutions[0:min(10,npa)]))


# ----------------------------------------------
# from all solutions, get subset of possible recommendations
#   - i.e. ties and very close 2nd's
# ----------------------------------------------
scoredRecommendations = lib.topGuesses(scoredSolutions)
nr = len(scoredRecommendations)
apiSend['numRecommendations'] = nr

if verbose:
    print("I have "+str(nr)+" top guesses for solutions.")
    print("Scored Recommendations (first 10) = "+str(scoredRecommendations[0:min(10,nr)]))


# ----------------------------------------------
# make the final recommendation
# ----------------------------------------------

# special case - the top choice has score 0 and there is more than 1
#  - we can't get any more info
if (scoredRecommendations[0][1] == 0 and nr > 1):
    apiSend['possArrow'] = 0
    alphaSolutions = sorted([ tup[0] for tup in scoredRecommendations ]) # sorted alpha list
    recommendation = alphaSolutions[possArrow]     # the nth word alphabetically
    if verbose: 
        print("I can't get any more info; I'm guessing from multiple answers.")
        print(alphaSolutions)
else:
    #  choice() returns the tuple, [0] selects the word
    recommendation = random.choice(scoredRecommendations)[0]

apiSend['recommendation'] = recommendation

if verbose: 
   print("The next guess recommendation word (method="+m+") = "+recommendation)


print(json.dumps(apiSend))
