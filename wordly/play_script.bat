rem batch script

rem a round is 2 parts:
rem   1. recommendation for next guess   - wordly
rem   2. guess & evalution               - wordlePlay

set answer=BANAL

python wordly.py "h=0&n=1&m=1&state="                                rem SALES
python wordlePlay.py "h=0&n=1&secret=%answer%&state=&guess=SALES"

python wordly.py "h=0&n=2&m=2&state=.A...|L|SE"                          rem BALMY
python wordlePlay.py "h=0&secret=%answer%&guess=BALMY&state=.A...|L|SE"

python wordly.py "h=0&n=3&m=2&state=BA...|L|SEMY"                        rem only 2 recommendations


rem ---------------------------------------------------------------------

set answer=JORGE

python wordly.py "h=0&n=1&m=1&state="                                rem round 1
python wordlePlay.py "h=0&n=1&secret=%answer%&state=&guess=ARISE"

python wordly.py "h=0&n=2&m=2&state=....E|R|AIS"                     rem NOTRE
python wordlePlay.py "h=0&secret=%answer%&guess=NOTRE&state=....E|R|AIS"


python wordly.py "h=0&n=2&m=2&state=.O..E|R|AISNT"                   rem FORGE
python wordlePlay.py "h=0&secret=%answer%&guess=FORGE&state=.O..E|R|AISNT"


python wordly.py "h=0&n=2&m=2&state=.ORGE||AISNTF"                   rem ties
