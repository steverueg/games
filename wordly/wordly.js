/* javascript for ajax capabilities

api is python
  - for py cgi: add 2 lines to end of xampp/apache/conf/httd.conf
      AddHandler cgi-script .py
      ScriptInterpreterSource Registry-Strict

*/

gameRoundState = '';   // ....|| string; match|out-of-position|naughty
gameState = '.....||';
gameOver = false;
round = 1;             // 1-based counting, first guess is #1;
playState = 0;    
possArrow = 0;         // edge-case

/* PlayState
      0 - waiting for "guess" button
      1 - waiting for "response" button
*/

function resetBoard() {
    // just like it sounds
    gameRoundState = '';   // ....|| string; match|out-of-position|naughty
    gameState = '.....||';
    gameOver = false;
    round = 1;             // 1-based counting, first guess is #1;
    playState = 0;    
    possArrow = 0;         // edge-case

    // loop over rounds / guesses
    for (var r=0; r<=6; r++) {
        if ( r >= 1) {
            var btn = document.getElementById('r'+r);
            btn.disabled = true;
            btn.innerHTML = "guess "+r;
        }
        // loop over chars in this guess
        for (var c=0; c<5; c++) {
            var id = "r"+r+"c"+c;
            var box = document.getElementById(id);
            box.innerHTML = '';
            box.style.backgroundColor="#FFF";
        }
    }
    log.innerHTML = `
----------------------------------- 
New Game
----------------------------------- 
You can enter a 5-letter word that the computer 
will try to guess. It will not know the answer,
you will provide the responses. 

You may also leave the word blank, and play Wordle
on your phone.  Enter the responses from your phone
into this app, and the computer will give you 
word recommendations. 
`;

}

/* --------------------------------------------------- 
   start New Game
   --------------------------------------------------- 
    - user clicked on blue button to start over with "new" word
*/
function startNewGame() {

    resetBoard();

    // put the user's answer on the board - top row
    var answer = document.getElementById('answer').value;
    console.log("start new game answer="+answer);

    // put chars in boxes
    for (var i = 0; i < 5; i++) {
        var id = "r0c"+i;
        var box = document.getElementById(id);
        var ai = (answer[i] == undefined) ? "?" : answer[i].toUpperCase() ;
        box.innerHTML = ai.toUpperCase();
        box.style.backgroundColor="#DDF";
      }

    // reset globals
    gameOver = false;
    round = 1;
    document.getElementById('r1').disabled = false;
    playState = 0;     // waiting for "guess" request
    gameState = ".....||";
}


/* --------------------------------------------------- 
   manage play State
   --------------------------------------------------- */
function play(btn) {
    // the user clicked one of the 6 game play buttons
    // - these can be "guess" (state=0) or "respond" (state=1) button 

    console.log("play event occured:  playState = "+playState)
    if (gameOver) { return; }

/* PlayState
      0 - waiting for "guess" button
      1 - waiting for "response" button
*/

    if (playState == 0) {
        // user clicked on "guess"

        // disable current guess button (just clicked)
        document.getElementById('r'+btn).disabled = true;
        document.getElementById('r'+btn).innerHTML = "thinking";
        log.innerHTML += `
-------------------------
   thinking
-------------------------
`;
        for (var i = 0; i < 5; i++) {
            var id = "r"+round+"c"+i;
            document.getElementById(id).innerHTML = '*';
        }

        getNextGuess(btn);  // call the API
        playState = 1;      // now we let the user to formulate the response
        return;
    }

    if (playState == 1) {
        // user clicked on "response"
        // check that all chars have a response

        // get gameRoundState: .....||
        match = ['.','.','.','.','.'];   //5
        oop = "";
        naughty = "";
        for (var i = 0; i < 5; i++) {
            var id = "r"+round+"c"+i;
            var char = document.getElementById(id).innerHTML;
            var rstate = document.getElementById(id).getAttribute('rstate');
            console.log("id="+id+"; char="+char+"; rstate="+rstate);
            if (rstate == 0) {
                console.log("id="+id+"; char="+char+"; rstate="+rstate);
                log.innerHTML += "Not all chars have a response.\n"
                return;
            }
            if (rstate == 3) { match[i] = char; }
            if (rstate == 2) { oop += char; }
            if (rstate == 1) { naughty += char; }
        }
        gameRoundState = match.join('')+"|"+oop+"|"+naughty; // global
        console.log("gameRoundState = "+gameRoundState);
        log.innerHTML += "gameRoundState = "+gameRoundState+"\n";

        // disable response button - this round is done
        document.getElementById('r'+round).disabled = true;

        // is game over???
        if (round == 6) {
            gameOver == true;
            playState = 2;  // game over
            console.log('Game Over');
            log.innerHTML += `
-----------------------------------
  game over - all 6 attempts were made
  -----------------------------------
`;
            return;
        }


        // ===================================================
        // now we change to the NEXT ROUND
        // ===================================================
        playState = 0;      // back to waiting for guess request
        round = round + 1; 
        document.getElementById('r'+round).disabled = false;
        return;
    }

    // Never get here - each if-then has its own return;

}


function isPerfectMatch() {
    // match char for char only
    var count = 0;
    for (var i = 0; i < 5; i++) {
        var aid = "r0c"+i;
        var gid = "r"+round+"c"+i;
        var achar = document.getElementById(aid).innerHTML;
        var gchar = document.getElementById(gid).innerHTML;
        if (achar == gchar) { count += 1; }
    }
    console.log("This guess matches = "+count);
    return( count == 5 );  // boolean - Perfect match or not?
}


/* --------------------------------------------------- 
  get the recommendation from the wordly algo
   --------------------------------------------------- 
   - the user clicked the guess # button
*/
function getNextGuess() {
    // user pushed "guess" button -- call the API wordly-as-a-service

    // define URL which sends game state to API
    //  - on bitnami on aws: use wordly.php
    var URL = "wordly.py?n="+round+"&m=2&state="+gameState+"&response="+gameRoundState;
    if (possArrow > 0){ URL += "&possArrow="+possArrow; }
    console.log("URL="+URL);

    // make the API call to wordly.py
    //  -- wait for recommendation to be returned 
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // send the response hash (apiSend) to the log <span>
            // log is document.getElementById('log'); 
            // reset/clear the log; start over for this round
            log.innerHTML = "Response:\n"+xhr.response;

            // get the actual recommendation
            //   -- the response is all the stdout text from python
            //   -- including debugging
            //   -- the LAST line is JSON hash
            var loglines = xhr.response.split("\n");
            var jsonLine = loglines[loglines.length - 2].trim();
            var responseHash = JSON.parse(jsonLine);   // JSON object
            gameState = responseHash['state'];         // global
            console.log("JSON="+JSON.stringify(responseHash));
            processRecommendation(responseHash);
        }
    };

    xhr.open("GET", URL, true);
    xhr.send();
}


/* --------------------------------------------------- 
   process async return of api call
   --------------------------------------------------- 
    - put recommendation into board
    - prompt user to respond to guess
*/
function processRecommendation(responseHash) {
    // the api has returned with its guess
    if (gameOver) { return; }
    console.log("process API recommendation for round="+round);

    // error?
    if (responseHash['Error'] == true) {
        console.log("Error in response.");
        gameError(responseHash);
    }

    // put chars on board
    updateBoard(responseHash['recommendation'])
    var btn = document.getElementById('r'+round);

    // perfect match?? 
    if (isPerfectMatch()) {
        gameOver == true;
        playState = 2;  // game over
        console.log('perfect match');
        log.innerHTML += `
---------------------------------------------
Horray!  I guessed correctly!   Game Over.
---------------------------------------------
`;
        btn.innerHTML = "Game Over";
        for (var i = 0; i < 5; i++) {
            var id = "r"+round+"c"+i;
            document.getElementById(id).style.backgroundColor="#4E4";
        }
        return;
    }
    
    // ready for user to respond
    //  - update the game state global var
    //  - we are waiting for user to click response
    btn.innerHTML = "response";
    btn.disabled = false;

    playState = 1;
    log.innerHTML += `
Now you click on each letter to set the color.
  1x - Grey = Not IN
  2x - Yellow = IN, but out of position
  3x - Green = Matched Perfectly
`;

    // edge-case processing
    if (responseHash['possArrow'] != undefined) {
        possArrow = parseInt(responseHash['possArrow']) + 1;
        console.log("possArrow incremented to "+possArrow);
    }

}



function gameError(reponseHash) {
    // end graciously
    log.innerHTML += `
------------------------
game Error
------------------------
Something is inconsistent.  I cannot give a recommendation
`;
    log.innerHTML += reponseHash['ErrMsg'];
    var btn = document.getElementById('r'+round);
    btn.innerHTML = "game over;"
    gameOver == true;
    playState = 2;  // game over
}



/* --------------------------------------------------- 
   update the board 
   --------------------------------------------------- */
function updateBoard(guess) {
    // put the api recommendation (i.e. guess) on the board
    // add 'rstate' attribute: 0=null; 1=not; 2=out-of-position; 3=match
    // all this is during playState = 0
    for (var i = 0; i < 5; i++) {
        var g = guess[i];
        var id = "r"+round+"c"+i;
        var clickedCell = document.getElementById(id);
        clickedCell.innerHTML = g;
        clickedCell.setAttribute('rstate',0);
        clickedCell.style.backgroundColor="#FFF";  // just to be sure
    }
}



/* --------------------------------------------------- 
   update_rstate
   --------------------------------------------------- */
// add 'rstate' attribute: 0=null; 1=not; 2=out-of-position; 3=match
// playState = 1
function update_rstate(rid) {
    // the api recommendation (i.e. guess) is on the board
    // user is responding to the guess cmp to the secret answer
    // add 'rstate' attribute: 0=null; 1=not; 2=out-of-position; 3=match
    if (gameOver) { return; }
    if (rid[1] != round) { return; }
    if (playState != 1) { return; }

    // get current response state and update by 1 (mod 4 = 0..3)
    var clickedCell = document.getElementById(rid);
    var r0 = parseInt(clickedCell.getAttribute('rstate'));
    var r2 = (r0 + 1) % 4;
    clickedCell.setAttribute('rstate', r2);
    console.log("state="+playState+"; cell="+rid+"; r0="+r0+" r2="+r2);
    if (r2 == 0) { clickedCell.style.backgroundColor="#FFF"; }
    if (r2 == 1) { clickedCell.style.backgroundColor="#DDD"; }
    if (r2 == 2) { clickedCell.style.backgroundColor="#FF0"; }
    if (r2 == 3) { clickedCell.style.backgroundColor="#5F5"; }

}
