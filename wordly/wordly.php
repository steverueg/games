<?php 
/* python wrapper
    - bitnami lamp does not include apache mods for python
    - that's fine... its actually a security
    - so, we'll use php to wrap the python wordly

# test with: 
#  (datascience) C:\Users\steve\src\games\wordly>python wordly.py "state=....E&|A|KSJ&n=1&h=0"
*/

# project root
$project = "/opt/projects/games/wordly";

# get game input states
$n = $_GET['n'] ?? '';
$m = $_GET['m'] ?? '';
$state = $_GET['state'] ?? '';
$response = $_GET['response'] ?? '';
$possArrow = $_GET['possArrow'] ?? '';

# create command line argument string
$args = "n=$n&m=$m&state=$state&response=$response";
if ($possArrow != ''){
    $args += "&possArrow=$possArrow";
}

# make wrapper call
system("/usr/bin/python3 $project/wordly.py 'h=0&$args'");

?>