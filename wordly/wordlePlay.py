#!C:/Users/steve/anaconda3/python.exe
# ---------------------------------------
# this evaluates a play: the guess against the answer
# ---------------------------------------
# input: guess & answer
# output: the resultState
#
#  - for py cgi: add 2 lines to end of xampp/apache/conf/httd.conf
#      AddHandler cgi-script .py
#      ScriptInterpreterSource Registry-Strict
# 
# test with: 
#  python api/cgi args:  https://stackoverflow.com/questions/25336688/run-python-script-from-ajax-or-jquery
#  (datascience) C:\Users\steve\src\games\wordly>python wordlePlay.py "secret=ARISE&guess=BATES"

import cgi, json
import wordly_lib as lib

verbose = False

# ----------------------------------------------
# init 
# ----------------------------------------------
apiSend = {}      #  What we send to HTML/JS in json format

# get current game initilization parameters
data = cgi.FieldStorage()

# setup API 
h = data.getvalue('h', '1')
if (h != '0'):
    print("Content-Type: text/html")
    print()

# get secret answer & guess 
secret = data.getvalue('secret', '').upper()
guess = data.getvalue('guess', '').upper()
if secret == '' or guess == '':
    print("ERROR: secret and guess are not input")
    exit(1)

if verbose:
    print("secret="+secret)
    print("guess="+guess)

resultStr = lib.evaluate_guess(guess, secret)

apiSend['secret']    = secret
apiSend['guess']     = guess
apiSend['resultStr'] = resultStr


# previous states represents all previous results
state0 = data.getvalue('state', '').upper()
if state0 == '': state0= '.....||'            # init new game

# update game state with this play result
state = lib.aggResultSet([state0, resultStr])

if verbose:
    print("result = "+resultStr)
    print("updated game state = "+state)

apiSend['state0'] = state0
apiSend['state'] = state

print(json.dumps(apiSend))
