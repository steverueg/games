<!DOCTYPE html>
<html lang="en">

<?php 
# ----------------------------------------
#  This is Home page
# ----------------------------------------

require("games_lib.php");
startHTML();
#topbar_navigation();

# start main page container
print("<div class='row'><div class='col'>\n");
instructions("Here is a twist on popular games.  I created Games-as-a-service 
for these 4 games.  The computer will 'play against' you; or search for the 
best solution given the current game state. Click anywhere on the game
'tile' (or 'card') to start a new game.  Enjoy -- it's kinda fun.", 
    $title="Games as a service");

# a row for the cards
print("<div class='row ml-5'>\n");

# ---------------------------------------
# Hangman
# ---------------------------------------
$hangman = array();
$hangman['title'] = "Hangman";
$hangman['text'] = "You pick a secret word, and the computer will guess it.";
$hangman['img'] = "hangman/hm7.png";
$hangman['url'] = "hangman/hmaas.html"; 
gameCard($hangman);


# ---------------------------------------
# tic-tac-toe
# ---------------------------------------
$ttt = array();
$ttt['title'] = "Tic-Tac-Toe";
$ttt['text'] = "Play against the computer";
$ttt['img'] = "tic-tac-toe/ttt.png";
$ttt['url'] = "tic-tac-toe/tic-tac-toe.html"; 
gameCard($ttt);

# ---------------------------------------
# MasterMind
# ---------------------------------------
$mastermind = array();
$mastermind['title'] = "MasterMind";
$mastermind['text'] = "You pick a 4-marble secret, and the computer will guess your sequence based on your clues.";
$mastermind['img'] = "mastermind/mm.png";
$mastermind['url'] = "mastermind/mastermind.php"; 
gameCard($mastermind);


# ---------------------------------------
# Sudoku
# ---------------------------------------
$sudoku = array();
$sudoku['title'] = "Sudoku";
$sudoku['text'] = "Build-your-own sudoku board. The computer will validate it, and print a PDF of your puzzle with one of the possible solutions.";
$sudoku['img'] = "sudoku/sudoku.png";
$sudoku['url'] = "sudoku/sudoku.php"; 
gameCard($sudoku);


# ---------------------------------------
# Wordly
# ---------------------------------------
$wordly = array();
$wordly['title'] = "Wordle";
$wordly['text'] = "The computer will guess 'your' 5-letter word.  OR you can enter responses from a Wordle-app and it can give you recommendations.";
$wordly['img'] = "wordly/wordly.png";
$wordly['url'] = "wordly/index.html";
gameCard($wordly);


# end row of cards
print("</div> <!-- row of cards -->\n\n");

print("</div></div>");   # end main page container: row & col
endHTML(); 
?>

