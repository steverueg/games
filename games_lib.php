<?php

session_start();  

# --------------------------------------------------------------------------------
#  standard HTML functions
# --------------------------------------------------------------------------------
function startHTML($redirect=null) {
    # print the HTML from <head> to <body> 
    # bootstrap 4.5.3 
    # wget https://github.com/twbs/bootstrap/releases/download/v4.5.3/bootstrap-4.5.3-dist.zip
    # jquery 3.5.1
    # wget https://code.jquery.com/jquery-3.5.1.min.js
  
    if (isset($redirect)) {
      print <<<EOP
<head>
  <meta http-equiv="Refresh" content="0; URL='$redirect'">
</head>
EOP;
      exit;
    }
  
    print <<<EOP
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title> NAMADE Games-AAS </title>
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
EOP;
#    <style> body { padding-top: 56px; } </style>

}
  
  
  

# --------------------------------------------------------------------------------
function footer(){
    # footer for every page 
    # bootstrap 4.5.3 
    # wget https://github.com/twbs/bootstrap/releases/download/v4.5.3/bootstrap-4.5.3-dist.zip
    # jquery 3.5.1
    # wget https://code.jquery.com/jquery-3.5.1.min.js

    $fp = fopen("version.txt", "r");
    if (isset($fp)) {
      $version = rtrim(fgets($fp));
      fclose($fp);
    }
    

    $txt = <<<EOP
<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; S. Ruegsegger 2021  (app v$version)</p>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
EOP;
    return $txt;
}



  # --------------------------------------------------------------------------------
  function endHTML() {
    # closes out the main page_content, adds the footer, and ends the HTML tags too
 
    print( footer() ); 
    print("\n</body>\n</html>\n");
  }
  
  
  
# --------------------------------------------------------------------------------
function topbar_navigation() {
    # immediately follows <body> 
    global $clubName;
    $thisurl = urlencode($_SERVER['REQUEST_URI']);

    print "\n<!-- Navigation -->\n";
    print <<<EOP
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="home.php"> $clubName </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item"><a class="nav-link" href="home.php">Home</a></li>
        <li class="nav-item"><a class="nav-link" href="contact.php?thisurl=$thisurl">Contact</a></li>
        <li class="nav-item"><a class="nav-link" href="2fa_otp_setup.php">2FA Setup</a></li>
        <li class="nav-item"><a class="nav-link" href="info.php">About</a></li>
        <li class="nav-item"><a class="nav-link" href="index.php?logout=true">Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

EOP;
}



function instructions($text, $title=null){
    # a common look for page instructions
    if (isset($title)){
      print("<h2 style='text-align:center'>$title</h2>\n");
    }
    print <<<EOP
  <div class="row p-3 mb-5" style='background-color:#e6e6ff'><div class="col"><em>
  $text
  </em></div></div>
  EOP;
  
  }

  
function gameCard($instance) {
    # print out 1 game card - instance is a hash
    $url = $instance['url'];
    print <<<EOP
<div class="col-lg-4 col-md-6 mb-4">
    <div class="card" style="width: 18rem;" onClick='window.location="$url";'>
    <div class="card-header"> <h5 style='text-align:center'>{$instance['title']} </h5></div>
        <img class="card-img-top" src="{$instance['img']}" alt="Card image cap">
        <div class="card-body">
            <p class="card-text">{$instance['text']}</p>
        </div>
    </div>
</div>
EOP;
}


