<?php 
/* python wrapper
    - bitnami lamp does not include apache mods for python
    - that's fine... its actually a security
    - so, we'll use php to wrap the python sudoku-aas
*/

$verbose = true;

# ----------------------------------
# 1. setup 
# ----------------------------------
# get board string
$b = $_GET['b'] ?? null;

# project root
# $project = "C:\Users\steve\src\games\sudoku";  # xampp
$project = "/opt/projects/games/sudoku";         # bitnami on AWS

# python executable
# $py3 = 'C:/Users/steve/anaconda3/envs/datascience/python.exe';  # xampp
$py3 = '/usr/bin/python3';                                        # bitnami on AWS


# ----------------------------------
# 2. py api call
# ----------------------------------
# the arguments from php wrapper to py api
#  - linux can have single quotes
#  - windows (xampp) required double quotes
$arg = "\"h=0&b=$b\"";
#if ($verbose) { print("PHP arg to py api = $arg\n"); }

# run py3 api
#  - the py api prints out json
#  - the php system() command just repeats it
#  - and it is the api output which is read in by js. 
if ($verbose) { print("system('$py3 $project/sudoku-aas.py $arg', \$rc);\n\n"); }
system("$py3 $project/sudoku-aas.py $arg", $rc);

if ($rc > 0) {
    # if you print this, then JS cannot read the json string from the 
    # system() call above.  SO, rc!=0 should infer NO JSON
    print("Note: py3 error RC=$rc in sudoku-aas.php \n\n");
}
# else we are OK

?>