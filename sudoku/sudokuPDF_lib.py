# PDF export library for sudoku-aas

#  - aws lightsail needed: $ sudo apt install python3-matplotlib

import matplotlib
import matplotlib.pyplot as plt


# -------------------------------------------------------
#  template
# -------------------------------------------------------
def makeGrid(axis, scale=1.0):
    """Draw the standard, common template for sudoku problem and answer on 8.5x11 portrait paper
       The axis is a subplot.
    """

    # 1. draw the largest object
    #    - it fills the page
    #    - it sets the scale: 100x100
    #    - it is an invisible box
    axis.add_patch(matplotlib.patches.Rectangle((0,0), 100, 100, facecolor='none'))

    # set up the thick and thin line positions
    l3 = [ round(100*i/3,2) for i in (1,2) ]
    l1 = [ round(100*i/9,2) for i in (1,2,4,5,7,8) ]
    
    # scale down if needed 
    max = int(100*scale)
    l3 = [ round(x*scale,2) for x in l3 ]
    l1 = [ round(x*scale,2) for x in l1 ]

    # the outer box of the sudoku grid
    axis.add_patch(matplotlib.patches.Rectangle((0,0), max, max, facecolor='none',ec='C0', ls='-'))
    # the thick and thin lines
    for pct in l3:
        axis.vlines(pct,0,max,linewidth=3)
        axis.hlines(pct,0,max,linewidth=3)
    for pct in l1:
        axis.vlines(pct,0,max,linewidth=1)
        axis.hlines(pct,0,max,linewidth=1)


# -------------------------------------------------------
#  popualate boards with specifics to this game
# -------------------------------------------------------

def cellNumtoPDFxy(cellNum):
    "given a cell number (0 to 80), return the x,y position to then insert/draw the number"
    
    cellx = cellNum%9        # col - remainder of a group
    celly = int(cellNum/9)   # row - the group number 
    #print(f"cell #{cellNum} = x,y = col,row = {cellx},{celly} -- all 0-based")

    # grid calculations 
    offset = round(100*1/18,2)
    multiple = round(100*1/9,1)
    yt = round((8-celly)*multiple+offset,2)-2   # cells count top-down, but plt is (0,0) at bottom-up
    xt = round(cellx*multiple+offset,2)-1       # both L-to-R

    return (xt,yt)


def insertBoard(axis, boardStr, scale=1.0):
    "given a 81-char string of a 9x9 board, insert all numbers"
    
    for cellNum in range(len(boardStr)):
        value = boardStr[cellNum]
        if value == '0': continue
        (xt,yt) = cellNumtoPDFxy(cellNum)
        #print(f"cellNum={cellNum},  xt={xt}, yt={yt} (non-scale)")
        
        # scale 
        xt = round(xt*scale, 2)
        yt = round(yt*scale, 2)
        fs = 'large' if (scale == 1.0) else 'medium'
        axis.text(xt,yt, value, fontsize=fs)





# -------------------------------------------------------
#  export PDF file
# -------------------------------------------------------
def PDFexport(boardStr, solutionStr, fname):
    "given a board and a solution (as 81-char strings), export to a 1-page PDF"

    # game board on top, solution on bottom
    fig, axs = plt.subplots(2,figsize=(8, 11))
    fig.suptitle('My Sudoku')

    # top game board
    axs[0].axis('off')
    axs[0].set_title('my puzzle')
    makeGrid(axs[0])

    # bottom solution (scaled)
    axs[1].axis('off')
    #axs[1].set_title('a solution')
    axs[1].text(35, 85, 'a solution', fontsize='large')    # at top of 80% scale
    makeGrid(axs[1], 0.8)

    # Enter the problem numbers
    insertBoard(axs[0], boardStr)

    # Enter the solution numbers
    insertBoard(axs[1], solutionStr, scale=0.8)

    # draw and export 
    # plt.show()
    fig.savefig(fname)


