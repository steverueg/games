/* javascript for ajax capabilities*/


thinking = false;
numberRadio = null;   // 0-9 radio button
selectedCell = null;
lastCell = null;
aSlnStr = null;
numHints = 0;     



/* --------------------------------------------------- 
   These 9 numbers 1-9 are radio buttons.  They are highlighted 
   one-at-a-time to define the number to be set into a table
   cell when clicked.
   --------------------------------------------------- */
   function numIn(id) {
    // user has clicked one of the buttons

    if (thinking) { return; }

    numberRadio = id;  // set global var for all the subsequence clicks

    // radio highlight - all off + 1 on
    console.log("highlighting number "+id);
    for (var i=1; i<10; i++) {
        var clickedCell = document.getElementById('numin'+i);   // each table cell;
        var clr = (i==id) ? "#EEE" : "#FFF";
        clickedCell.style.backgroundColor = clr; 
    }

    // reset board with new instructions
    log = document.getElementById('log');
    log.innerHTML = "\nClick on all the "+id+"'s all over this Sudoku board.\n";

}



/* --------------------------------------------------- 
  the user selected a cell to place a hint
   --------------------------------------------------- */
function placeHint(id) {
    // user clicked on a cell to update
    // these are numbered 0-80:  0-based counting for 9x9=81

    if (thinking) { return; }    // just ignore clicks if game over
    console.log("user clicked on cell #"+id);

    // update board cell with the previously selected number radio
    //   - set or clear
    var cell = document.getElementById('cell'+id);
    cell.innerHTML = (cell.innerHTML == '') ? numberRadio : '';

    // update numHints global var AND db summary (after changing cell)
    clearDB();
    numHintsUpdate();

    if (numHints == 10) {
        msg  = '--------------------------------------------------------\n';
        msg += 'You now have enough hints to evaluate a solution.\n';
        msg += '--------------------------------------------------------\n\n';
        log.innerHTML = msg + log.innerHTML;  // prepend
    }

    // clear out selectedCell (user can't just keep clicking on numbers)
    lastCell = selectedCell;
    selectedCell = null;
    bgReset();
    
}



/* --------------------------------------------------- 
   make the API ajax call
   --------------------------------------------------- */
   function sudokuAAS() {
    // the user clicked blue Update button -> call API
    thinking = true;

    // clear out log & summary dashboard
    log = document.getElementById('log');
    log.innerHTML = '';
    document.getElementById('updateBtn').disabled = true;
    clearDB();

    // too few hints message
    if (numHints < 10) {
        msg  = '--------------------------------------------------------\n';
        msg += 'There are too few hints to process a solution.\n';
        msg += 'Once you have 10 hints, then push the blue Update button.\n';
        msg += '--------------------------------------------------------\n\n';
        log.innerHTML = msg;
    }

    // get the board state
    var boardStr = getBoard();

    // define URL which sends game state to API
    //   - use .py  for xampp on laptop
    //   - use .php for bitnami on aws
    var URL = "sudoku-aas.py?b="+boardStr;
    console.log("URL="+URL);

    // make the API call to Sudoku_AAS
    //  -- wait for solution evaluation to be returned 
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // send the text to the log <span>
            log = document.getElementById('log');
            log.innerHTML = log.innerHTML + xhr.response;
            document.getElementById('updateBtn').disabled = false;

            // get an error action -- the last line
            var loglines = xhr.response.split("\n");
            var jsonLine = loglines[loglines.length - 2].trim();
            const response = JSON.parse(jsonLine);        // JSON object
            console.log("JSON="+JSON.stringify(response));
            processJSONresponse(response);
            }
        };

    var msg = document.getElementById('msg');
    msg.innerHTML = "thinking...";
    xhr.open("GET", URL, true);
    xhr.send();

}


/* --------------------------------------------------- 
   action functions
   --------------------------------------------------- */
function invalidMove() {
    // the API told us the last move was invalid!
    var clickedCell = document.getElementById("cell"+lastCell);
    clickedCell.style.backgroundColor="#FDD";
    console.log("OOps!  That move was bad.");
    log = document.getElementById('log');
    log.innerHTML = log.innerHTML + "OOps!  That was a bad move.  Clear it and try again."

}

function clearDB() {
    // clear out the summary dashboard
    document.getElementById("db1").innerHTML = "";
    document.getElementById("db2").innerHTML = "";
    document.getElementById("db3").innerHTML = "";
    document.getElementById("db4").innerHTML = "";
    aSlnStr = null;
    document.getElementById("export").innerHTML = "";  // div under board

}

function processJSONresponse(response) {
    // response is a parsed JSON object from the python API

    thinking = false;
    // turn off msg
    var msg = document.getElementById('msg');
    msg.innerHTML = " ";
    
    // update dashboard
    var db1 = document.getElementById("db1");  // hints
    var db2 = document.getElementById("db2");  // valid
    var db3 = document.getElementById("db3");  // fix
    var db4 = document.getElementById("db4");  // solution

    db1.innerHTML = response.hints;
    db2.innerHTML = response.valid;
    db3.innerHTML = response.fix;
    db4.innerHTML = response.solution;

    // if the board is not sudoku-valid highlight the (1st) invalid row,col, or box
    if (!response.valid) {
        var fixType = response.fix[0];
        var fixNum = response.fix[1];
        console.log("FIX needed: type="+fixType+"  num="+fixNum);
        hiliteFix(fixType, fixNum);
    }

    // display export button
    aSlnStr = response.aSlnStr;                     // hold for async export (81-char string)
    var xport = document.getElementById("export");  // div under board
    if (response.valid && parseInt(response.hints) >= 10) {
        xport.innerHTML = "<button type=button class='btn btn-success' onClick='exportPDF();'>PDF export</button>";
    }
    else {
        xport.innerHTML = "";   
    }
}


function exportPDF() {
    // user clicked the green button -> send the export PHP script to _blank
    var boardStr = getBoard();
    var URL = "exportPDF.php?b="+boardStr+"&s="+aSlnStr;
    console.log("export URL="+URL);
    window.open(URL, '_blank');
}


function hiliteFix(type, num) {
    // highlight the cells (row, col, box) which have invalid entry

    if (type=='row') {
        for (let i = 0; i < 9; i++) {
            n = 9*num + i;  // a group of 9
            document.getElementById('cell'+n).style.backgroundColor="#FEE";
        } 
    }

    if (type == 'col') {
        for (let i = 0; i < 9; i++) {
            n = 9*i + num;  // all groups, with same offset
            document.getElementById('cell'+n).style.backgroundColor="#FEE";
        } 
    }

    if (type == 'box') {
        // a box is only 9 of these 81 cells
        for (let n=0; n<81; n++) {
            var r=Math.floor(n/9);
            var c=n%9;
            console.log("r="+r+"  c="+c);
            if ((Math.floor(r/3)*3 + Math.floor(c/3)) != num) { continue; }
            document.getElementById('cell'+n).style.backgroundColor="#FEE";   
        }
    }

}




/* --------------------------------------------------- 
   helper functions
   --------------------------------------------------- */

function bgReset() {
    // remove last bgcolor, by resetting all 9
    for (let i = 0; i < 81; i++) {
        document.getElementById('cell'+i).style.backgroundColor="#FFF";
        } 
}


function numHintsUpdate() {
    // user is changing cell to have num in it 
    numHints = 0;   // global js var (reset)
    for (let i = 0; i < 81; i++) {
        var c = document.getElementById('cell'+i).innerHTML;
        if (c!="" && c!=" "){ numHints += 1;}
      } 
    console.log("numHints="+numHints);
    document.getElementById('db1').innerHTML = numHints;
}


function getBoard() {
    // convert the HTML board to a string of .01
    var boardStr = '';
    for (let i = 0; i < 81; i++) {
        var c = document.getElementById('cell'+i).innerHTML;
        if (c=="" || c==" "){ boardStr += "0";}
        else { boardStr += c; }
      } 
    console.log("boardStr="+boardStr);
    return boardStr;
}

