#!C:/Users/steve/anaconda3/python.exe
#  - for py cgi: add 2 lines to end of xampp/apache/conf/httd.conf
#      AddHandler cgi-script .py
#      ScriptInterpreterSource Registry-Strict

# test with: 
#  python api/cgi args:  https://stackoverflow.com/questions/25336688/run-python-script-from-ajax-or-jquery
# (datascience) C:\Users\steve\src\games\sudoku>python sudoku-aas.py "b=800010009050807010004090700060701020508060107010502090007040600080309040300050008"

#  b=800010009050807010004090700060701020508060107010502090007040600080309040300050008
#  b=L68vGQAwdA4gAD5rTAA545XAHkhhywAKA/ygAGtuWABMlrMAEeJmWA
#  b=000010000050000010000090700000001020000000107010500000007000000000000000300000000
#  b=AAAnEAAvrwigAAFiTAAAAD/AAAAAawAKA3oAAGrPwAAAAAAAEeGjAA
#  b=000000000000000000000000000000000000123456789000000000000000000000000000000000000
#  b=AAAAAAAAAAAAAAAAAAAAAAAAB1vNFQAAAAAAAAAAAAAAAAAAAAAAAA

import sys, cgi
import sudoku_lib as lib
import json
verbose = True

# setup json response
apiSend = { 'hints': None,
            'valid': False,
            'fix': None,
            'solution': None,
            'aSlnStr' : None
}


# ----------------------------------------------
# get current game initilization parameters
# ----------------------------------------------
# the board, b, is 81 chars 0..9
#   - it could be represented as 54-symbol base64 encoding
data = cgi.FieldStorage()
boardStr = data.getvalue('b', '')
h = data.getvalue('h','1')

# setup API
if (h != '0'):
    # if testing this script by-itself, print a header
    print("Content-Type: text/html")
    print()


# convert 54 char base64 to 81 char base10 string
if len(boardStr) == 54:
    boardStr = lib.base64tobase10(boardStr)
    if (verbose): print("board="+boardStr)


# valid board string (length)
if len(boardStr) != 81:
    if verbose: print("len="+str(len(boardStr)))
    print(json.dumps(apiSend))
    sys.exit(0)        # exit with rc=0 for JS to read json

# ----------------------------------------------
# board setup
# ----------------------------------------------
board = lib.ajax2board(boardStr)
#if verbose: lib.printBoard(board)

hints = lib.numConstraints(board)
apiSend['hints'] = hints
if verbose: 
    print("There are "+str(hints)+" hints in this sudoku board.")

# are the board clues valid for sudoku
if (not lib.is_board_valid(board)):
    if verbose: print("The board is not Sudoku valid")
    fix = lib.findFix(board)
    apiSend['fix'] = fix
    print(json.dumps(apiSend))
    sys.exit(0)        # exit with rc=0 for JS to read json

if verbose: print("The board is valid for Sudoku")
apiSend['valid'] = True

# don't continue if less than 10 hints
if hints < 10:
    print("When there are < 10 hints, I won't process the board. Let's get more than 10.")
    apiSend['solution'] = 'too few hints, min=10'
    print(json.dumps(apiSend))
    sys.exit(0)        # exit with rc=0 for JS to read json



# ----------------------------------------------
# how many solutions to you find
# ----------------------------------------------
# ss is a hash where the key is 81-char string and the value is the count
solutionSet = lib.multipleSolution(board)
numSS = len(solutionSet.keys())
numCompletions = 0
for k,v in solutionSet.items():
    numCompletions += v
sln = 'unique solution' if (numSS==1) else 'different solutions'
msg = f"{numSS} {sln} from {numCompletions} completions"
apiSend['solution'] = msg

# record solution in api
if numSS == 0:
    apiSend['solution'] = "0 - There are no solutions for these hints!"
else:
    aSlnStr = list(solutionSet.keys())[0]
    apiSend['aSlnStr'] = aSlnStr
    if verbose: 
        print("\n\n"+msg+".  One example is...")
        lib.printBoard(lib.ajax2board(aSlnStr))

print(json.dumps(apiSend))
