#!/usr/bin/python3
#!C:/Users/steve/anaconda3/envs/datascience/python.exe

#  This is a wrapper for PHP to call py3 b/c bitnami and xampp 
#  do not call py3 (matplotlib well at all!) 
#   - note the venv above to get matplotlib import
#   - this script recieves a board and a solution as 81-char string
#   - it is given the temp filename 
#   - it creates a PDF temp file -- that's it, nothing is returned

import cgi, sys
import sudokuPDF_lib as lib
verbose = True


# get initilization parameters
#  - the board and solution are 81-char strings
#  - the fname is what this script produces 
data = cgi.FieldStorage()
boardStr = data.getvalue('b', '')
solutionStr = data.getvalue('s', '')
fname = data.getvalue('f', '')


# error check
#if verbose: print("fname="+fname+"   b="+boardStr+"   s="+solutionStr)
if fname == '': sys.exit(1)
if len(boardStr) != 81: sys.exit(2)
if len(solutionStr) != 81: sys.exit(3)


lib.PDFexport(boardStr, solutionStr, fname)
sys.exit(0)
