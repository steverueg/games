<?php 
/* python wrapper
    - bitnami lamp does not include apache mods for python
    - that's fine... its actually a security
    - so, we'll use php to wrap the python sudoku-aas

    if you get a hard 'not found' error, it's probably
    because the tmp space for the pdf file is NOT world writeable.

*/

$verbose = false;

# ----------------------------------
# 1. setup 
# ----------------------------------

# get board and solution string
$b = $_GET['b'] ?? null;
$s = $_GET['s'] ?? null;
$h = $_GET['h'] ?? '0';

if ($h != '0') {
    # debugging --> for any h (non-default 0)
    print("Content-Type: text/html\n\n");
    $verbose = true;
}

# project root
$project = '/opt/projects/games/sudoku';

# python executable
$py3 = '/usr/bin/python3';

# generate a file name
#   - in linux, put full path with 777 mods
$r = rand(1E6,1E7-1);
$fname = "/tmp/saas/saas{$r}.pdf";

if ($verbose) {
    print("b=$b\n");
    print("s=$s\n");
    print("fname=$fname\n");
    print("project=$project\n");
    print("py3=$py3\n");
}


if (empty($b) || empty($s)) {
    print("something is wrong with inputs\n\n");
    exit(9);
}

# ----------------------------------
# 2. py api call
# ----------------------------------

# the arguments from php wrapper to py api
#  - linux can have single quotes
#  - windows (xampp) required double quotes
$arg = "\"h=0&f=$fname&b=$b&s=$s\"";
if ($verbose) { print("PHP arg to py api = $arg\n"); }

# run py3! 
#  - have the py api create the temp filename
if ($verbose) { print('system("$py3 $project/exportPDF.py $arg", $rc);\n\n'); }
system("$py3 $project/exportPDF.py $arg", $rc);
if ($verbose) { print("py done\n"); }


if ($rc > 0) {
    header("<!DOCTYPE html>");
    header("<html lang='en'>");  
}
# else we are OK


# ----------------------------------
# 3. return PDF file
# ----------------------------------
# now, have PHP send the pdf to web browser for download
# - https://www.php.net/manual/en/function.header.php

header('Content-Type: application/pdf');
header('Content-Disposition: attachment; filename=my_sudoku.pdf');
ob_clean();
flush();
readfile($fname);

?>

