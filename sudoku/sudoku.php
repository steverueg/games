<!DOCTYPE html PUBLIC"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
<style>
h1,h2 {
  text-align: center;
}
td {
  width: 50px;
  height: 50px;
  text-align: center;
}
table {
  margin: 5px auto;
}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
<h1>Sudoku As A Service</h1>

<div class='row ml-3'>
    <div class='col-md-6'>


<?php


# ------------------------------------------------------------
# numeric 1-touch input
# ------------------------------------------------------------
#  - once a sudoku board cell is selected, a number from here is
#    placed into that cell as a hint
print("
<p><b>Instructions:</b> Click on a number (1-9) to highlight and 'set'
that number to be loaded into the board based on your all your next
'clicks' in the Sudoku table. To clear out a cell, click on it again --
and the first number will clear away. You need at least 10 hints to 
even start a solution search. </p>

<table border=1>
<tr>
");

 foreach (range(1,9) as $ni) {
  print("  <td id='numin${ni}' onClick='numIn($ni);'>$ni</td>\n");
}
print("</tr>\n");
print("</table><br><br>\n\n");

# ------------------------------------------------------------
# 1-touch sudoku table 
# ------------------------------------------------------------
#  - if selected, cell is hilited (bgcolor) and the next number is placed
#    in the cell as a hint
print("<table id='sudoku' border=1>\n");

foreach (range(0,8) as $row) {
  $bb = ($row == 2 || $row == 5) ? "style='border-bottom:3pt solid black;'" : "";
  print(" <tr $bb>\n");
  foreach (range(0,8) as $col) {
    $br = ($col == 2 || $col == 5) ? "style='border-right:3pt solid black;'" : "";
    $i = $row*9 + $col;
    print("  <td $br id='cell$i' onClick='placeHint($i);'></td>\n");
  }
  print(" </tr>\n");
}
print("</table><br>\n\n");

?>
<div id='export' style='text-align:center'></div>

</div>  <!-- col -->

<div class='col-md-6'>

<!-- ------------------------------------------------------------
  dashboard in top right 
  ------------------------------------------------------------ -->
<table border=1 style='margin:5px;'>
  <tr><th style='width:100px;background-color:#EEF;'># Hints</th>    <td id='db1' style='width:200px;'>0</td></tr>
  <tr><th style='width:100px;background-color:#EEF;'>Valid?</th>     <td id='db2' style='width:200px;'></td></tr>
  <tr><th style='width:100px;background-color:#EEF;'>Fix</th>        <td id='db3' style='width:200px;'></td></tr>
  <tr><th style='width:100px;background-color:#EEF;'>Solution(s)</th><td id='db4' style='width:200px;'></td></tr>
  <tr><td colspan=2><button id='updateBtn' class='btn btn-info' type='button' onClick='sudokuAAS();'>Update Solution</button></td></tr>
</table>
<br>

<h2 id='msg'style='margin:auto'> </h2>
<br>

<pre id='log'>
    Sudoku as a service
</pre>


</div> <!-- col -->
</div> <!-- row  -->

<script src="sudoku-aas.js"></script>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
