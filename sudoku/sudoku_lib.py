#  library for sudoku-aas

import random, base64
verbose = True

MAXN = int(1E5)     # max iteration number 100K
ATTEMPTS = 10       # num loops looking for a soluion

# -------------------------------------------------------
# helper functions
# -------------------------------------------------------
# base64 is a 6-bit conversion from 4-bit bytes 
# each 9-cell row on the board is a group
def base64tobase10(b64Str):
    "take a 54-char base64 string convert to 81-char base10 string"
    boardStr = ""
    for g in range(0,9):
        groupB64Str = b64Str[6*g : 6*(g+1)] + "=="
        groupB64 = groupB64Str.encode('ascii')          # string into ascii chars  
        groupBytes = base64.b64decode(groupB64)         # char-to-num: encode using base64
        groupInt = int.from_bytes(groupBytes,'big')     # one big num from array
        groupStr = "{:09d}".format(groupInt)            # int to padded string
        boardStr += groupStr
    return boardStr


def base10tobase64(b10Str):
    "take a 81-char base10 string convert to 54-char base64 string"
    boardB64Str = ""       # init
    for g in range(0,9):
        groupStr = b10Str[9*g : 9*(g+1)]
        groupInt = int(groupStr)                      # string to single int
        groupBytes = groupInt.to_bytes(4,'big')       # int to byte-array: 4 bytes needed for 10^9      
        groupB64 = base64.b64encode(groupBytes)       # encode bytes to symbols: 4*8 = 32 bits (byte-array) into 6 groups of 6 (b64)
        groupB64Str = groupB64.decode('ascii')[:-2]   # b64 wants groups of 3-bytes (3*12-bits or 36-bits) we can remove the last 2 = pads
        boardB64Str += groupB64Str
    return boardB64Str


def ajax2board(boardStr):
    "convert a string of 81 chars to 9x9 2D array of ints"
    board = []
    for x in range(0,9):
        row = []
        for y in range(0,9):
            p = x*9 + y
            row.append(int(boardStr[p]))
        board.append(row)
    return board

def board2Str(board):
    "given a 9x9 array, convert to 81 characters 0..9"
    boardStr = ""
    for rows in board:
        for c in rows:
            boardStr += str(c)
    return boardStr


def printBoard(board):
    "given a 9x9 array, ascii print the values, with 9 3x3 squares highlighted"
    for y in range(0,9):
        if y in (3,6):
            print("+".join(["-------"]*3))
        
        # then loop over cols in this row (each X)
        for x in range(0,9):
            if x == 0: print(" ",end='')
            v = board[y][x]
            if v == 0: p = "."
            else: p = str(v)
            print(p+" ",end='')
            if x in (2,5): print("| ", end='')
        print("")



# -------------------------------------------------------
# back-tracing play evaluation logic
# -------------------------------------------------------

def used_in_row(arr,row,num):
    "return True if num is ALREADY being used in row"
    for i in range(9):   
        if(arr[row][i] == num): return True
    return False

def used_in_col(arr,col,num):
    "return True if num is ALREADY being used in col"
    for i in range(9):  
        if(arr[i][col] == num): return True
    return False

def used_in_box(arr,row,col,num):
    "return True if num is ALREADY being used in a 3x3 box"
    for i in range(3):
        for j in range(3):
            if(arr[i+row][j+col] == num): return True 
    return False

def check_location_is_safe(arr,row,col,num):
    """return True if its OK/valid to put num at this [row][col], otherwise return FALSE becuase num at r,c would be a violation. 
       if any of the 3 tests are TRUE, then we negate to FALSE; and just 1 FALSE will make the ANDs fail."""
    return not used_in_row(arr,row,num) and \
           not used_in_col(arr,col,num) and \
           not used_in_box(arr,row - row%3,col - col%3,num)


def find_empty_location(arr,l):
    """given a board, find 'any' empty cell.  Rather than always sequential, use a
       randomly generated list sequence to find other solutions
    """
    global rows, cols
    for row in rows:
        for col in cols:
            if(arr[row][col]==0):
                # update l to be the next empty location
                l[0]=row
                l[1]=col
                return True
    return False



# -------------------------------------------------------
# The recursive backtracing function
# -------------------------------------------------------
global n, rows, cols, guesses

def solve_sudoku(arr):
    """only change the board and continue if a valid number can be placed in an empty spot. 
      if we get done checking options and empty spaces exist, we return False.
    """
    global n, rows, cols, guesses

    # max number of iterations... some solutions paths are not good
    if n > MAXN: return False 

    # a new list place holder, changed by the function below to an empty cell
    l = [ None, None ]

    # if board is filled, then we are done
    #  - l is updated an empty locaton
    if (not find_empty_location(arr,l)): return True
    
    # next empty location, and a list of our guesses to try
    row = l[0]
    col = l[1]

    for num in guesses: 
        if(check_location_is_safe(arr,row,col,num)): 
            arr[row][col] = num 
            n = n+1
            # recursive!!!! 
            if(solve_sudoku(arr)): return True 
            # failure -- no future soltion on this branch!
            #  - unmake that cell & try again 
            arr[row][col] = 0 
    
    return False




# -------------------------------------------------------
# A loop to search for N non-unique solutions
# -------------------------------------------------------
def multipleSolution(board):
    """Try to find multiple solutions.  Use random number selection to explore different trees."""
    global n, rows, cols, guesses

    solutionSet = {}
    for i in range(ATTEMPTS):
        # make a copy of original board
        array81 = [[]] * 9
        for x in range(9):
            array81[x] = [ y for y in board[x] ]

        # prep globals for this solution path attempt
        #print("\nready to try solution attempt "+str(i+1)+" of 10")
        n = 0
        rows = list(range(9))
        random.shuffle(rows)
        cols = list(range(9))
        random.shuffle(cols)
        guesses = list(range(1,10))
        random.shuffle(guesses)
        if False:
            print("guess order = "+str(guesses))
            print("row order = "+str(rows))
            print("col order = "+str(cols))

        # start recursive back-tracking algo to find a solution
        #  - there are 3 types of results:
        #     1. valid solution (repeat or unique)
        #     2. no solution (n < MAXN)
        #     3. null solution (I quit trying at MAXN)
        if (solve_sudoku(array81)): 
            # --------------------------------------------------------
            # result #1
            print("solution search attempt #"+str(i)+" = FOUND in "+str(n)+" steps: ", end='')
            boardStr = board2Str(array81)
            if boardStr in solutionSet: 
                if verbose: print("Existing solution found again.")   # 1b
                solutionSet[boardStr] += 1
            else: 
                if verbose: print("New solution found!")              # 1a
                solutionSet[boardStr] = 1
        else:
            # --------------------------------------------------------
            if n > MAXN:
                # result #3
                if verbose: print("solution search attempt #"+str(i)+" HALTED after "+str(n)+" steps")
            else:
                # result #2
                if verbose: print("solution search attempt #"+str(i)+" UNSUCCESSFUL after "+str(n)+" steps")
                break          # 1 unsuccessful = all will be

    return solutionSet



# -------------------------------------------------------
# board-level logic
# -------------------------------------------------------

# check for valid starting point 
def is_board_valid(board):
    array = [[]] * 9
    for x in range(9):
        array[x] = [ y for y in board[x] ]
    
    validBoard = True
    for row in range(9):
        for col in range(9):
            num = 0 + array[row][col]  # copy
            if num == 0: continue
            array[row][col] = 0
            result = check_location_is_safe(array,row,col,num)
            # false means there is a violation.  True means num at [row][col] is valid
            #print(f"checking array row={row} col={col} num={num} result={result}")
            array[row][col] = num     # back to regular value 
            validBoard = validBoard and result
    return validBoard



def numConstraints(board):
    """count how many constraints -- fewer contraints means more possible solutions. 
       from empirical simulations, 16 or fewer clues all have multiple solution.  17 clues starts possible unique solutions
    """
    numConstraints = 0
    for row in range(9):
        for col in range(9):
            if board[row][col] != 0: numConstraints = numConstraints + 1
    return numConstraints




def findFix(board):
    "The board is invalid, return the first row or col or box with the violation"
    # 1. row = y = 1st dimension
    for y in range(9):
        flags = [ False ]*10
        for x in range(9):
            num = board[y][x]
            if num !=0 and flags[num] == True: return ('row',y)  # seen twice!
            flags[num] =  True  # if seen once, then True

    # cols = x = 2nd dimension
    for x in range(9):
        flags = [ False ]*10
        for y in range(9):
            num = board[y][x]
            if num !=0 and flags[num] == True: return ('col',x)  # seen twice!
            flags[num] =  True  # if seen once, then True

    # box
    for b in range(9):
        flags = [ False ]*10
        for x in range(9):
            for y in range(9):
                # y = row; x = col; get the row,col group of 3, then count by 3x3
                if int(y/3)*3 + int(x/3) != b: continue
                num = board[y][x]
                if num !=0 and flags[num] == True: return ('box',b)  # seen twice!
                flags[num] =  True

    return None


