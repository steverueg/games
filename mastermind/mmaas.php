<?php 
/* python wrapper
    - bitnami lamp does not include apache mods for python
    - that's fine... its actually a security
    - so, we'll use php to wrap the python t3aas

*/

# project root
$project = "/opt/projects/games/mastermind";

# get board string
$guess = $_GET['guess'] ?? '';
$response = $_GET['response'] ?? '';

system("/usr/bin/python3 $project/mmaas.py 'h=0&guess=$guess&response=$response'");

?>