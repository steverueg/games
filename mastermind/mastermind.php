<!DOCTYPE html PUBLIC"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
<style>
h1,h2 {
  text-align: center;
}
td {
  width: 100px;
  height: 50px;
  text-align: center;
}
table {
  margin: 5px auto;
  border: 1px solid black;
}
.vert {
  border-left: 2px solid black;
  border-right: 2px solid black;
}
.hori {
  border-top: 2px solid black;
  border-bottom: 2px solid black;
}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
<h1>Mastermind As A Service</h1>

<?php

$colors = [ 1=>'red', 2=>'orange', 3=>'green', 4=>'blue', 5=>'purple', 6=>'yellow' ];

# ----------------------------------------
# define secret
# ----------------------------------------
function getSecretPulldowns() {
  # one pulldown in each table cell
  global $colors;
  foreach (range(0,3) as $position) {
    $pd = "\n<td id='secretCell${position}'><select id='secret${position}'>";
    foreach (range(1,6) as $clr){
      $pd .= "<option value=$clr>$clr-".$colors[$clr]."</option>";
    }
    $pd .= "</select></td>";
    print($pd);
  }
}

# ----------------------------------------
# define responses
# ----------------------------------------
function getResponsePulldowns($round, $position) {
  # two response pulldowns (not in individual table cells)
  $pd = "\n<select id='response${round}${position}'>";
  foreach (range(0,4) as $correct){
    $pd .= "<option value=$correct>$correct";
  }
  $pd .= "</select>";
  print($pd);
}

?>


<div class='row'>
    <div class='col-md-6'>

<form>

<table id='solution' class='table ml-3'>
  <tr class='hori'>
    <td class='vert'>secret</td>
    <?php getSecretPulldowns(); ?>
  </tr>
</table>
<p id='msg' class='ml-3'>You pick a secret code. You can pretend
  each number 1-6 is a color of a marble.  You are hiding 4 marbles
  from me. 
</p>
<p class='ml-3' style='text-align:center'><button type=button class='btn btn-success' id='getGuess' onClick='playStateEvent();'>Set Secret Code</button></p>

<?php

# ----------------------------------------
# draw board
# ----------------------------------------
# rounds and cols are 0-based counting in code

print("<table id='board' class='table ml-3'>
<tr style='background-color:#eeeeee;text-align:center'><th>play #</th><th colspan=4>computer's guess</th><th colspan=2>Your Response</th></tr>
");
foreach (range(0,6) as $row) {
  $round = $row + 1;
  print("<tr class='hori'>");
  print("<td class='vert'>play=$round</td>");
  foreach(range(0,3) as $col) {
    print("<td id='r${row}c${col}'> x </td>");
  }
  print("<td class='vert' id='cell${row}0'>inPlace");
  getResponsePulldowns($row,0);
  print("</td>\n<td id='cell${row}1'>outPlace");
  getResponsePulldowns($row,1);
  print("</td></tr>\n");
}
print("</table>");



?>

</form>

</div>  <!-- col -->

<div class='col-md-6'>
<pre id='log' style='white-space: pre-wrap;'>
 First, define the secret code of marble colors.  
 Trust me, "I" don't know your secret code. 
</pre>
</div> <!-- col -->

</div> <!-- row  -->

<script src="mmaas.js"></script>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
