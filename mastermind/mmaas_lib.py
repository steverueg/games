#  library for mmaas
#  - 3 unit entities:  secret, guess, response
#  - a play is a guess + response pair
#  - a playList is a series of plays (g+r lists)

import random
verbose = False


def firstGuess():
    "return list of random ints with pattern: x,x,y,y"
    [x,y] = random.sample(range(1,6),2)
    p = [x,x,y,y]
    random.shuffle(p)
    return p



#-------------------------------------------------
# complete sets of solutions and responses
#-------------------------------------------------
#  4 spaces of 6 colors

def getSolutionSet():
    "return the set of 1296 possible secret codes"
    row = [1,1,1,1]
    solutionSet = []

    # 4 spaces of 6 colors
    for c0 in range(1,7):
        row[0] = c0
        for c1 in range(1,7):
            row[1] = c1
            for c2 in range(1,7):
                row[2] = c2
                for c3 in range(1,7):
                    row[3] = c3
                    newarray = []                 # copy the array
                    newarray = [x for x in row]   # new memory-location array copy
                    solutionSet.append(newarray)

    return solutionSet

# very pythonic list of all possible responses 
possibleResults = list(filter( lambda r: r[0]+r[1] <=4,  [ [x,y] for x in range(0,5) for y in range(0,5) ] ))
solutionSet = getSolutionSet()


#-------------------------------------------------
# calculate the response to a play
#-------------------------------------------------
# The algo needs this when simulating future plays to come up with a cost 


cntp = [0]*7
cnts = [0]*7

def getResponse(s, p):
    "given current secret and a new play, return the response"
    global cntp, cnts

    # reset memory locations 0..6
    for x in range (0,7): 
        cntp[x]=0
        cnts[x]=0

    # 1. check for correct colors first
    for m in s: cnts[m] += 1    # e.g. a '3' increases count of 3rd element
    for m in p: cntp[m] += 1
    correctColorCnt = 0
    for n in range(1,7): correctColorCnt += min(cnts[n], cntp[n])
    #print(f"#s={cnts}, #p={cntp}")
     
    # 2. check Correct Position 
    correctPositionCnt = 0
    for m in range(0,len(p)): 
        if s[m] == p[m]: correctPositionCnt += 1
            

    correctColorCnt -= correctPositionCnt
    #print("there were "+str(correctColorCnt)+" marbles with the correct color, but not in the correct position")
    #print("there were "+str(correctPositionCnt)+" marbles in the correct position")

    return [correctPositionCnt, correctColorCnt]


#-------------------------------------------------
# could a past response allow a particular solution to be the secret?
#-------------------------------------------------
#   - that is, based on a past play = guess+response pair, could a potental
#     solution be the secret?

def isValidSecret(play, solution):
    """given 1 play -- a guess and its response -- could a potential solution exist
       or did that guess+response play eliminate it?"""
    [guess, response] = play
    simResponse = getResponse(solution, guess)
    return simResponse == response



#-------------------------------------------------
# determine the set of secrets which are still possible after N plays
#-------------------------------------------------
# this first function starts at play 1 and full secret list
# the second function starts at play N and subset
def getSecretSubset(playList):
    """given a game's playList, starting at play #1, return the set of possible, viable secrets
       after removing the secrets which rejected by previous plays = guess + response"""
    rounds = len(playList)
    
    # no plays yet -- the subset is really the entire solution set (1296)
    if rounds == 0: return solutionSet

    solutionStart = [None] * rounds  # the secret subset after each play 
    solutionStart[0] = solutionSet   # init/start with all possible solutions

    # loop over each play and only keep only valid solutions
    for n in range(0,rounds):
        p = playList[n]    # play = guess + response
        secretSubset = []  # valid secrets after this play

        # start with previous solution set, only keep valid solutions
        for s in solutionStart[n]:
            if isValidSecret(p, s): secretSubset.append(s)

        # this solution is the starting point for the next play
        #print("There are "+str(len(solutionSubset))+" possible solutions after play #"+str(p))
        if n < rounds-1: solutionStart[n+1] = secretSubset  

    return secretSubset
    

def getNextSecretSubset(nextPlay, secretSubset):
    """knowing that the first N plays results in secret subset, 
       return the small set of possible, viable secrets for 1 more (additional) play"""
    nextSecretSubset = []
    # start with previous solution set, only keep valid solutions
    for s in secretSubset:
        if isValidSecret(nextPlay, s): nextSecretSubset.append(s)
    return nextSecretSubset



#-------------------------------------------------
# score for 1 possible next guess
#-------------------------------------------------
# - add the next guess to the playList
# - cross-product with the 15 possible responses (we don't know the actual secret)
# - for those 15 plays (next guess * 15 resposnes) get it's possible Secret Set 
# - how many of the current secret set are eliminated by the new secret set????
# - the MIN of all 15 eliminated secrets is the score 

def getGuessScore(nextGuess, secretSubset):
    """Given a possible next guess, return its score -- aggregated pver all possible 15 responses.
       The score is ...
        the min # of current possible Secrets that would be eliminated by this next Guess
        each of the 15 can return # secrets from 0 to current secret count.  
        the elimination is simply current secret count - new, simulated secret count
        if sss is empty, that just means there is an inconsistancy in the response and we ignore it
    """
    currSecretCnt = len(secretSubset)
    guessScore = currSecretCnt
    for r in possibleResults:
        nextPlay = [nextGuess, r]        # the next guess and 1 of the 15 possible responses
        sss = getNextSecretSubset(nextPlay, secretSubset)
        # the score is min # eliminated (>0)
        if sss != []: guessScore = min(guessScore, currSecretCnt-len(sss))
        
    return guessScore
    
