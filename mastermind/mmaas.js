/* javascript for ajax capabilities

api is python
  - for py cgi: add 2 lines to end of xampp/apache/conf/httd.conf
      AddHandler cgi-script .py
      ScriptInterpreterSource Registry-Strict

*/


gameOver = false;
round = 0;
guessStr = "";
responseStr = "";


/* --------------------------------------------------- 
   manage play State
   --------------------------------------------------- */
playState = -1;   
   // 0 = waiting for get next guess
   // 1 = process next guess
   // 2 = wait for response 
   // 3 = process response
   // go back to 0

function playStateEvent() {
    // the user did something in the game 
    console.log("event occured:  playState = "+playState)
    if (gameOver) { return; }

    if (playState == -1) {
        // set secret code
        for (var i = 0; i < 4; i++) {
            sid = 'secret'+i;
            cid = 'secretCell'+i;
            s = document.getElementById(sid).value;
            img = "<img src='m"+s+".png'>";
            document.getElementById(cid).innerHTML = img;
        }
        playState = 0;
        document.getElementById('msg').innerHTML = "Ready for first guess from computer"; 
        document.getElementById('getGuess').innerHTML = "get 1st guess"; 
        return;
    }


    if (playState == 0) {
        // user clicked to get next guess
        document.getElementById('getGuess').disabled = true; 
        document.getElementById('msg').innerHTML = "thinking..."; 
        guess = getNextGuess();   // async return
        return;
    }

    if (playState == 2) {
        // user entered response (playState=3)
        correctPos = document.getElementById("response"+round+"0").value;
        correctClr = document.getElementById("response"+round+"1").value;
        n = parseInt(correctPos) + parseInt(correctClr);
        if (n > 4) {
            alert("The responses cannot add up to more than 4.");
            return; 
        }

        // update global state
        response = correctPos+correctClr;   // string 
        if (round == 0) { responseStr = response; }
        else { responseStr = responseStr+"+"+response; }
        console.log("guessStr="+guessStr+"   responseStr="+responseStr);

        document.getElementById("cell"+round+"0").innerHTML = correctPos;
        document.getElementById("cell"+round+"1").innerHTML = correctClr;


        if (response == "40") {
            gameOver = true;
            document.getElementById('msg').innerHTML = "I got it!  In "+(round+1)+" guesses."; 
            document.getElementById('getGuess').innerHTML = "Game Over";
            document.getElementById('getGuess').disabled = true;
            return;
        }

        // change to playState=0
        playState = 0;
        round = round + 1; 
        document.getElementById('msg').innerHTML = "Ready for next guess"; 
        document.getElementById('getGuess').innerHTML = "get next guess"; 
        return;
    }

}



/* --------------------------------------------------- 
  get the recommendation from the mmaas algo
   --------------------------------------------------- */
function getNextGuess() {
    // enter playState=0, leave in playState=1
    // user has pushed green button to get next guess recommendation from api

    // define URL which sends game state to API
    var URL = "mmaas.php?guess="+guessStr+"&response="+responseStr;
    if (round == 0){ URL = "mmaas.php"; }  // wierd bug for empty GET
    console.log("URL="+URL);

    // make the API call to T3AAS
    //  -- wait for recommendation to be returned 
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // send the text to the log <span>
            log.innerHTML = xhr.response;
            // get the actual recommendation  -- the last line
            var loglines = xhr.response.split("\n");
            var apiCell = loglines[loglines.length - 2].trim();
            processAPI(apiCell);
        }
    };

    xhr.open("GET", URL, true);
    xhr.send();

}


/* --------------------------------------------------- 
   process async return of api call
   --------------------------------------------------- */
function processAPI(guess) {
    // the api has returned with its guess
    if (gameOver) { return; }
    if (guess == "-1") {
        console.log("game over due to inconsitency");
        gameOver = true;
        document.getElementById('msg').innerHTML = "There is an inconsistency. There is no possible solution based on these plays."; 
        document.getElementById('getGuess').disabled = true; 
        document.getElementById('getGuess').innerHTML = "game over"; 
        return;
    }


    playState = 1;
    console.log("playState=1, round="+round+": mmaas guess is "+guess);
    updateBoard(guess);

    // update global state 
    if (round == 0) { guessStr = guess; }
    else { guessStr = guessStr+"+"+guess; }
    console.log("guessStr="+guessStr+"   responseStr="+responseStr);

    // change to playState=2
    playState = 2;
    document.getElementById('msg').innerHTML = "<b>Grade this guess.</b> Your response has 2 parts: <ol><li># of elements in correct position (green background) <li># elements which exist, but incorrect position</ol>"; 
    document.getElementById('getGuess').disabled = false; 
    document.getElementById('getGuess').innerHTML = "response entered"; 
}


/* --------------------------------------------------- 
   update the board 
   --------------------------------------------------- */
function updateBoard(guess) {
    // put the api recommendation on the board  (stay in playState=1)
    for (var i = 0; i < guess.length; i++) {
        g = guess[i];
        img = "<img src='m"+g+".png'>";
        id = "r"+round+"c"+i;
        document.getElementById(id).innerHTML = img;

        // hint
        sid = 'secretCell'+i
        img = document.getElementById(sid).innerHTML;   // changed in playState=1
        s = img.substring(11,12);                          //<img src='mn.png'>
        console.log("updateBoard:  s="+s+" g="+g)
        if (s==g) {
            document.getElementById(id).style.backgroundColor="#CEC";
        }
      }
}

