#!C:/Users/steve/anaconda3/python.exe

# mastermind 
#  - 4 marbles of 6 colors
#  - 12 tries
#  - Bob Knuth's algorithm
#  - game state = list of previous guesses and reponses
#  - this algo returns a guess as output, the input is the state (old guesses, and new response)

#  - for py cgi: add 2 lines to end of xampp/apache/conf/httd.conf
#      AddHandler cgi-script .py
#      ScriptInterpreterSource Registry-Strict

# test with: 
#  python api/cgi args:  https://stackoverflow.com/questions/25336688/run-python-script-from-ajax-or-jquery
#  (datascience) >python.exe mmaas.py "guess=1234+3456+1256&response=11+01+21"

import sys, random
import cgi

import mmaas_lib as lib

verbose = True



# ----------------------------------------------
# get current game initilization parameters
# ----------------------------------------------
data = cgi.FieldStorage()
guessStr = data.getvalue('guess', '')         # a string, representing a list of lists
responseStr = data.getvalue('response', '')   # a string, representing a list of lists
h = data.getvalue('h','1')


# setup API
if (h != '0'):
    print("Content-Type: text/html")
    print()

if verbose: print("guessStr="+guessStr+"\nresponseStr="+responseStr)

# ----------------------------------------------
# 1. state setup
# ----------------------------------------------
# string to list
pastGuesses = []
for x in guessStr.split(' '):
    g = [ int(c) for c in x ]
    if len(g) != 4: continue
    pastGuesses.append(g)   # list of lists

#if verbose: print("guess="+str(pastGuesses))

pastResponses = []
for x in responseStr.split(' '):
    r = [ int(c) for c in x ]
    if len(r) != 2: continue
    pastResponses.append(r)   # list of lists

#if verbose: print("response="+str(pastResponses))

playList = []
for i in range(0,len(pastGuesses)):
    play = [pastGuesses[i], pastResponses[i]]
    playList.append(play)

if verbose: 
    n = len(playList)
    print("1. This is play round #"+str(n+1))
    print("current playList = "+str(playList))

# special edge case:
#   the game start has the same state each time
#   so, we can simulate once.  There is a formula
#   for the 1st guess 
if playList == []:
    recommendation = lib.firstGuess()
    if verbose: 
        print("I have a special algo for a first guess.")
        print("my 1st move recommendation is: "+str(recommendation))
    print(''.join([str(x) for x in recommendation])) # just a string of chars
    sys.exit(0)



# 2. get valid potential secrets still available after current plays
#    - determine the set of secrets which are still possible after N plays
secretSubset = lib.getSecretSubset(playList)
n = len(secretSubset)

if n == 0:
    print("There is an inconsistency in this game.\nThere are no possible answers remaining based on the play list.")
    print("i.e. some response must be incorrect. Game Over.")
    print(-1)
    sys.exit(1)

if n == 1:
    print("I got this!  There is only 1 remaining possible solution.  This has to be your secret code!")
    recommendation = secretSubset[0]
    print(''.join([str(x) for x in recommendation])) # just a string of chars
    sys.exit(0)

if verbose: 
    print("2. Based on the plays, there are "+str(n)+" remaining possible secrets: "+str(random.sample(secretSubset, min(n,10)))+"...")



# 3. score the possible next guesses
#    - loop over every possible guesses (==> all 1296 solutions - previous guesses
#    - loop over all possible answers (8) for each of those guesses
#    - for each of those nested loops, how many additional possible solutionSubSet does that <b>remove<b>
guessScores = []

# loop over all 1296
for nextGuess in lib.solutionSet:
    if nextGuess in pastGuesses: continue
    guessScore = lib.getGuessScore(nextGuess, secretSubset)
    guessScores.append([guessScore, nextGuess])
    
if verbose: 
    n = len(guessScores)
    print("3. I just scored "+str(n)+" possible next guesses: "+str(random.sample(guessScores, min(n,6)))+"...")


# 4. sort the scores and get the min score
#   - score = benefit; max benefit= good
guessScores.sort(key=lambda x: x[0], reverse=True)
maxScore = guessScores[0][0]
maxSet = list(filter(lambda x: x[0]==maxScore, guessScores))




# 5. if possible, pick recommendation from current secret subset 
#    - just might pick the secret 
choiceSet = list(filter(lambda x: x in secretSubset, [x[1] for x in maxSet]))
if choiceSet == []: choiceSet = [x[1] for x in maxSet]

if verbose: 
    n = len(choiceSet)
    print("4 & 5. There are "+str(n)+" next guesses which I really like with a minimax score = "+str(maxScore))
    print(str(random.sample(choiceSet, min(n,6)))+"...")


# 6. pick one of the low scores 
#    - random choice if ties 
recommendation = random.choice(choiceSet)
if verbose: 
    print("my final recommendation is: "+str(recommendation))


print(''.join([str(x) for x in recommendation])) # just a string of chars

