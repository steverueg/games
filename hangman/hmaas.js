/* javascript for ajax capabilities

   game flow:
     1. user enters # letters; clicks Setup Game
     2. user clicks Next Guess (blue button) for a letter from the API
     3. user clicks either Red button or Green button
     4a.  process good guess
     4b.  process bad guess
     5. 

*/

// global storage (local)
gameOver = false;
getInput = false;  

numLetters = null;
guessedLetter = '';
guessNumber = 0;
badGuessNum = 0;

// states of the game (send to ajax)
//  - only the user knows the correct word
//  - the clue is in the html table
usedLetters = '';
clickedPositions = []  // an array for undo


/* --------------------------------------------------- 
    Setup Game with # letters in secret word
   --------------------------------------------------- */
function setLetterTable() {
    // init - setup the HTML table for letters of secret word

    // get input and disable button
    numLetters = document.getElementById('numLetters').value;
    n = parseInt(numLetters);
    if (isNaN(n) || n < 4 || n > 20)  {
        document.getElementById('msg').innerHTML = "Invalid number of letters. Must be between 4 and 20.";
        return;
    }

    document.getElementById('msg').innerHTML = "";
    if (guessNumber == 0) { 
        document.getElementById('msg').innerHTML = "Click the 'Next Guess' blue button to get the 1st guess."; } 

    document.getElementById('numLetters').disabled = true;
    btn = document.getElementById('numBtn');
    btn.innerHTML = "got it";
    btn.disabled = true;

    // instructions in log
    console.log("Creating table for "+n+" letters");
    txt  = "OK. Let's start a game. \n";
    txt += "You have selected a word with "+n+" letters.\n";
    txt += "I will try to guess it in 7 tries.\n";
    txt += "Click 'Get Next Guess' blue button to start the game with my first guess.";
    document.getElementById('log').innerHTML = txt;

    // create the HTML table in js
    htmlTable = "<table><tr>";
    for (i=0; i<numLetters; i++) { 
        htmlTable += "<td id='"+i+"' onClick='guessedLetterGoesHere("+i+");'> </td>";
    }
    htmlTable += "<td style='border:0px;'><button class='ml-3 btn btn-grey' type='button' id='undoBtn' onClick='undoGuess();' disabled=true>Undo</button></td>";
    htmlTable += "</tr></table>";
    document.getElementById('hmtable').innerHTML = htmlTable;

    // release the blue button
    document.getElementById('blueBtn').disabled = false;
}


/* --------------------------------------------------- 
    Next Guess blue button
   --------------------------------------------------- */
function getGuess() {
    // send the clue to api via ajax for next guess - async!
    // user clicked the blue button

    console.log('setting up for sending state variables to api');
    var clue = getClueArray().join('');

    var URL = "hmaas.py?clue="+clue+"&used="+usedLetters;      // xampp
    //var URL = "hmaas.php?clue="+clue+"&used="+usedLetters;   // AWS
    console.log("URL = "+URL);

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // xhr.response hold what the API printed out
            // 1. send the text to the log 
            log.innerHTML = xhr.response;
            // 2. get the actual recommended apiCell # -- the last line
            var loglines = xhr.response.split("\n");
            var apiResponse = loglines[loglines.length - 2].trim();
            // ----------------------------------------------------------
            // after async wait... api made a guess, now we process it
            // ----------------------------------------------------------
            processLetterGuess(apiResponse);
        }
    };
    xhr.open("GET", URL, true);
    xhr.send();

    // communicate with user - after the SEND, not the async receive
    document.getElementById('msg').innerHTML = "Thinking...";
    document.getElementById('instruct').innerHTML = "";
    document.getElementById('blueBtn').disabled = true;
}


function processLetterGuess(apiResponse) {
    // with the computer guess and determine where it goes or not
    // we were waiting for async return, and now it just happened 

    if (gameOver) { return; }
    apiLetter1 = apiResponse.substring(0,1);

    // no valid dictionary words = no valid guess
    if (apiLetter1 == '!') {
        console.log("the API returned '!' -- no more guesses");
        gameOverMsg("I have no words!  This word is <u>not</u> in my dictionary.");
        return;
    }

    // I have only 1 possible dictionary word to be the secret
    if (apiLetter1 == '*') {
        console.log("the API returned '*' -- only 1 possible solution left: "+apiResponse);
        // fill out the 1 answer i have
        for (i=0; i<numLetters; i++) {
            // the DOM id= is simply an integer 
            document.getElementById(i).innerHTML = apiResponse.substring(i+1,i+2);
        }
        gameOverMsg("I got it!  This <u>must</u> your word.  (It's all I got.)");
        return;
    }


    // increase play round counter
    guessNumber += 1;
    console.log("For guess #"+guessNumber+", the computer guesses: "+apiLetter1);

    // global storage so user can click to add letter to secret word
    guessedLetter = apiLetter1;        // local to global
    console.log('temp storage guessedLetter='+guessedLetter);

    // store as used letter  - state variable
    usedLetters += guessedLetter;
    document.getElementById('guessed').innerHTML = usedLetters;
    console.log('game state usedLetters = '+usedLetters);

    // communicate with user
    document.getElementById('msg').innerHTML = "Is there the letter: '"+guessedLetter+"' in your secret word?";
    document.getElementById('instruct').innerHTML = "Click on all the square(s) above where the letter '"+guessedLetter+"' goes in your secret word. Click the green 'Finished' when finished.  Click the red 'Not here' button for an incorrect guess.";


    // now we wait...
    //    the user determines if is the letter in the secret word?
    //    user clicks the table char positions if correct
    //    or the red 'not here' button if not

    // ready to get input from the user! either red or green to continue 
    document.getElementById('greenBtn').disabled = false;
    document.getElementById('redBtn').disabled = false;
    getInput = true;
    clickedPositions = [];  // reset

}



/* --------------------------------------------------- 
    guessed letter goes here   (click in table)
   --------------------------------------------------- */
function guessedLetterGoesHere(id) {
    // the user clicked in cell #id, meaning the current guess goes here
    // in-table cell click
    // multiple clicks allowed

    if (guessNumber == 0 || gameOver || !getInput ) {
        return;  // quietly
    }

    // record which letter position # was pushed
    clickedPositions.push(id);
    console.log('correct! cell #'+id+' is where the letter '+guessedLetter+' goes');
    console.log('clickedPositions = '+clickedPositions);

    // put on the board
    document.getElementById(id).innerHTML = guessedLetter;  // from global temp

    // undo is active
    document.getElementById('undoBtn').disabled = false;
    document.getElementById('greenBtn').disabled = false;
    document.getElementById('redBtn').disabled = true;

    // did computer win
    var clue = getClueArray().join('');
    if (clue.includes('.') == false) {
        gameOverMsg("Hooray.  The computer got the word!");
    }

}



/* --------------------------------------------------- 
    Process Good Guess  (green button)
   --------------------------------------------------- */
   function goodGuess() {
    // the user is done clicking/putting letters in the secret word

    // check that at least one letter placed 
    exist = false;
    for (i=0; i<numLetters; i++){
        l = document.getElementById(i).innerHTML;  // the id is just an int
        if (l == guessedLetter) { exist = true; }
    }
    if (!exist) {
        console.log("oops, user clicked goodGuess before entering any letters.");
        document.getElementById('instruct').innerHTML = "You have not entered the letter '"+guessedLetter+"' into your secret word yet.  Click on the actual square where the letter goes.";
        return;
    }

    getInput = false;
    console.log('done adding letters to secret word');
    document.getElementById('instruct').innerHTML = "Letters added to secret word. Ready for next guess";
    document.getElementById('msg').innerHTML = "Click 'Get Next Guess' blue button to continue.";
    
    document.getElementById('undoBtn').disabled = true;
    document.getElementById('greenBtn').disabled = true;
    document.getElementById('redBtn').disabled = true;
    document.getElementById('blueBtn').disabled = false;   // next guess only

}




/* --------------------------------------------------- 
    Process Bad Guess  (red button)
   --------------------------------------------------- */
   function wrongGuess() {
    // what to do when the user tells us the letter is not in the secret word
    // red button
    if (guessNumber == 0 || gameOver || !getInput ) {
        return;  // quietly
    }
    if (clickedPositions.length > 0){
        console.log("good letter accepted, wrongGuess not allowed");
        return;
    }

    getInput = false;
    badGuessNum += 1;
    console.log('Bad Guess #'+badGuessNum+'.');
    png = document.getElementById('png').src = 'hm'+badGuessNum+'.png';

    document.getElementById('undoBtn').disabled = true;
    document.getElementById('greenBtn').disabled = true;
    document.getElementById('redBtn').disabled = true;
    document.getElementById('blueBtn').disabled = false;   // next guess only

    if (badGuessNum == 7) {
        // Game over due to too many bad guesses!!!! 
        gameOverMsg("Nope, there is no '"+guessedLetter+"' <b>and the game is over.</b>");
        return;
    }

    document.getElementById('msg').innerHTML = "Nope, there is no "+guessedLetter+".";
    document.getElementById('instruct').innerHTML = "Click 'Next Guess' to continue.";
    
}



/* --------------------------------------------------- 
    support 
   --------------------------------------------------- */
function undoGuess() {
    // take off the last entry -- misclick
    if (guessNumber == 0 || gameOver || !getInput ) {
        return;  // quietly
    }

    id = clickedPositions.pop();
    document.getElementById(id).innerHTML = ' ';
    console.log('cell #'+id+' is removed');
    console.log('clickedPositions = '+clickedPositions);
    if (clickedPositions.length == 0) { 
        document.getElementById('undoBtn').disabled = true;
        document.getElementById('redBtn').disabled = false;
        return; 
    }
}


function getClueArray() {
    // put html table into js array
    var clue = [];
    for (let i = 0; i < numLetters; i++) {
        var c = document.getElementById(i).innerHTML;
        if (c==" "){ clue.push('.'); }
        else { clue.push(c); }
      } 
    console.log("clue="+clue);
    return clue;
}



function gameOverMsg(msg) {
    // a standard place to end the game 

    document.getElementById('blueBtn').innerHTML = "Game Over";
    document.getElementById('blueBtn').disabled = true;
    document.getElementById('redBtn').disabled = true;
    document.getElementById('greenBtn').disabled = true;
    gameOver = true;

    document.getElementById('msg').innerHTML = msg;
    document.getElementById('instruct').innerHTML = "Game Over. Refresh (F5) or <a href='hmaas.html'>click here to play again.</a>";

}


