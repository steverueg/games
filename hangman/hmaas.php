<?php 
/* python wrapper
    - bitnami lamp does not include apache mods for python
    - that's fine... its actually a security
    - so, we'll use php to wrap the python hmaas

# test with: 
#  (datascience) C:\Users\steve\src\games\hangman>python hmaas.py "clue=...g...&used=EANI"
*/

# project root
$project = "/opt/projects/games/hangman";

# get game input states
$clue = $_GET['clue'] ?? '';
$used = $_GET['used'] ?? '';

# make wrapper call
system("/usr/bin/python3 $project/hmaas.py 'h=0&clue=$clue&used=$used'");
?>