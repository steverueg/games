#!C:/Users/steve/anaconda3/python.exe
#!/usr/bin/python3
#  - for py cgi: add 2 lines to end of xampp/apache/conf/httd.conf
#      AddHandler cgi-script .py
#      ScriptInterpreterSource Registry-Strict

# test with: 
#  python api/cgi args:  https://stackoverflow.com/questions/25336688/run-python-script-from-ajax-or-jquery
#  (datascience) C:\Users\steve\src\games\hangman>python hmaas.py "clue=...g...&used=EANI"

import random, re
import cgi

import hmaas_lib as lib

verbose = True

# ----------------------------------------------
# init 
# ----------------------------------------------

# 1. get current game initilization parameters
data = cgi.FieldStorage()
h = data.getvalue('h', '1')

# setup API 
if (h != '0'):
    print("Content-Type: text/html")
    print()


clue = data.getvalue('clue', '').upper()
n = len(clue)
alreadyGuessed = data.getvalue('used', '').upper()
if verbose: print("clue="+clue+"   guessed="+str(alreadyGuessed))


# 2. read in dictionary (plural?)
WORDS = lib.loadDictionary(n)
if verbose: print("loaded "+str(len(WORDS))+" "+str(n)+"-char words (upcase) into a dict")


# ----------------------------------------------
# find solution set of possible secret words
# ----------------------------------------------

# 3. get the list of possible words in this dictionary
#    list of words
#     a) where the clue matches
#     b) but no already guess letter exists
dictionaryWords = list(filter(lambda x: re.fullmatch(clue, x), WORDS))

n = len(dictionaryWords)
if n <= 1: lib.edgeCase(dictionaryWords)

if verbose: 
    print("there are "+str(len(dictionaryWords))+" possible words from the dictionary which fit the clue '"+clue+"'")



# 4. remove words with already guessed negative letters 
#   - negative letters (only) from alreadyGuessed
badGuess = []
for c in alreadyGuessed:
    if c not in clue: badGuess.append(c)

possibleWords = []
for w in dictionaryWords:
    # vector element: true if particular bad Guess is NOT there
    vector = [ bg not in w for bg in badGuess ]
    # if all are true in vector -- then this dictionary word is indeed still a possible answer word!
    if all(vector): possibleWords.append(w)

n = len(possibleWords)
if n <= 1: lib.edgeCase(possibleWords)

if verbose: 
    print("there are "+str(n)+" possible words without already used letters")
    print(" e.g. "+str(random.sample(possibleWords,min(n,5)))+"...")



# ----------------------------------------------
# from that solution set, get a letter guess recommendation
# ----------------------------------------------

# 5. get the chars which fill in the dots 
#    dict of letters and freq of when it fills in a clue .
fillCharsFreq = lib.getFillChars(clue, possibleWords)
if verbose: print("fill Chars freq = "+str(fillCharsFreq))


# 6. normalize fillChar freq by English language commonality
fillCharsNorm =  {}
for c,f in fillCharsFreq.items():
    fillCharsNorm[c] = f * lib.commonality[c]/100   
    
if verbose: 
    print("Normalized = {"+", ".join([ k+":%0.1f"%(v) for k,v in fillCharsNorm.items() ])+"}")



# 7. sort the valid letter guesses -- returning just the sorted chars (keys)
sortedGuess = sorted(fillCharsNorm, key=lambda i: fillCharsNorm[i], reverse=True)
if verbose: print("sorted letters: "+str(sortedGuess))


# 8. remove already guessed valid letters
validGuess = list(filter(lambda x: x not in alreadyGuessed , sortedGuess))
if verbose:
    print("alreadyGuessed letters = "+alreadyGuessed)
    print("non-used, valid, sorted guesses:"+str(validGuess))



# ----------------------------------------------
# make final recommendation
# ----------------------------------------------
# always the first one?    Maybe randomly select from top N
N = 2
recommendation = random.choice(validGuess[0:N])
if verbose: print("next guess recommendation (from top "+str(N)+")= ...")
print(recommendation)


