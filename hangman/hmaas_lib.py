#  library for hmaas
import sys

# -------------------------------------- 
#  init
# -------------------------------------- 

verbose = True
commonality = {'E': 11.16, 'A': 8.5, 'R': 7.58, 'I': 7.54, 'O': 7.16, 'T': 6.95, 'N': 6.65, 'S': 5.74, 'L': 5.49, 'C': 4.54, 'U': 3.63, 'D': 3.38, 'P': 3.17, 'M': 3.01, 'H': 3.0, 'G': 2.47, 'B': 2.07, 'F': 1.81, 'Y': 1.78, 'W': 1.29, 'K': 1.1, 'V': 1.01, 'X': 0.29, 'Z': 0.27, 'J': 0.2, 'Q': 0.2}
filename = "dictionary.txt"


def loadDictionary(n=None):
    """given a filename, load words into a dictionary hash (btree)
        1-line = 1-word"""
    #start = time.time()
    global filename
    words = {}
    fp = open(filename)
    for w in fp.read().split('\n'):
        if n is not None and len(w) != n: continue
        words[w] = None
    fp.close()
    #delta = (time.time() - start) * 1000 
    #print("elapsed time = %0.1f ms"%(delta))
    return words


# -------------------------------------- 
#  clue processing
# -------------------------------------- 

def getFillChars(clue, possibleWords):
    "get chars that fill in possible words in clue positions of missing chars"
    fillCharsFreq = {}
    # loop over possible words
    for p in possibleWords:
        # for each . in clue
        for i in range(0,len(clue)):
            if clue[i] == '.':
                c = p[i]
                if c not in fillCharsFreq: fillCharsFreq[c] = 1
                else: fillCharsFreq[c] += 1
    return fillCharsFreq


# -------------------------------------- 
#  edge case processing
# -------------------------------------- 
def edgeCase(wordList):
    "if possible dict or secrets are 0 or 1, we don't need a clue but a special message"
    n = len(wordList)

    if n == 0:
        if verbose: print("I have no words...  There are 0 possible words from the dictionary which are consistent with your letter positions.")
        print("!")
        sys.exit(1)

    if n == 1:
        print("I got this!  There is only 1 remaining possible solution.  This has to be your secret word!")
        recommendation = wordList[0]
        print('*'+recommendation)
        sys.exit(0)
